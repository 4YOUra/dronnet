FROM python:3.6 AS base

COPY . /dronnet
WORKDIR /dronnet

RUN apt-get update && apt-get install -y libpq-dev libproj-dev gdal-bin
RUN pip install pipenv
RUN pipenv install --three


FROM node:9.9 AS static_builder

ARG API_URL=https://dronnet.herokuapp.com/api
COPY ui /ui
WORKDIR /ui
RUN npm install
RUN API_URL=$API_URL npm run build


FROM base

COPY --from=static_builder /ui/build /dronnet/ui/build
RUN pipenv run python manage.py collectstatic --no-input
RUN mkdir -p /dronnet/.local/share && \
    cp -r /root/.local/share/virtualenvs /dronnet/.local/share/virtualenvs


ENV DJANGO_SETTINGS_MODULE dronnet.settings.heroku
CMD pipenv run python manage.py migrate --settings=$DJANGO_SETTINGS_MODULE && \
    pipenv run gunicorn --bind 0.0.0.0:$PORT dronnet.wsgi
