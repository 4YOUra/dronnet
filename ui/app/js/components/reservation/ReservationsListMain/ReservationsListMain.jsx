import React from 'react';
import PropTypes from 'prop-types';
import s from './style.scss';
import ReservationsList from "../ReservationsList/ReservationsList";
import DateSelector from '../../utils/DateSelector/DateSelector';
import {Link} from "react-router-dom";
import SectionTitle from "../../utils/SectionTitle/SectionTitle";
import Conditional from "../../utils/Conditional/Conditional";
import HelpMessage from "../../utils/HelpMessage/HelpMessage";


export default class ReservationsListMain extends React.Component {
    static propTypes = {
        defaultReservationDate: PropTypes.object.isRequired,
        errorMessage: PropTypes.string,
        isFetching: PropTypes.bool,
        onlyOwn: PropTypes.bool,
        onDateFilterChange: PropTypes.func.isRequired,
        onEditButtonClick: PropTypes.func.isRequired,
        onReservationClick: PropTypes.func.isRequired,
        onReservationHover: PropTypes.func.isRequired,
        onResetMapClick: PropTypes.func.isRequired,
        reservations: PropTypes.array.isRequired,
        userType: PropTypes.string.isRequired,
    };

    static defaultProps = {
        errorMessage: '',
        isFetching: false,
        onlyOwn: false,
    };

    render() {
        return (
            <div>
                <SectionTitle text={this.props.onlyOwn ? 'Own Reservations' : 'Reservations'}
                              size={this.props.onlyOwn ? '5' : '4'}
                              withSpinner={this.props.isFetching}>
                    <Link to="/create-reservation" className="button is-primary">
                        New
                    </Link>
                </SectionTitle>

                <Conditional if={!this.props.errorMessage}>
                    <div className="level">
                        <div className="level-left">
                            <DateSelector
                                defaultDate={this.props.defaultReservationDate}
                                onDateChange={this.props.onDateFilterChange}
                            />
                        </div>
                        <div className="level-right">
                            <button className="button is-dark is-small"
                                    onClick={this.props.onResetMapClick}>
                                Full Map
                            </button>
                        </div>
                    </div>
                </Conditional>

                <ReservationsList {...this.props}/>

                <Conditional if={this.props.reservations.length}>
                    <HelpMessage margin>Hover over a username to see reservation drones</HelpMessage>
                </Conditional>

                <Conditional if={!this.props.isFetching && !this.props.reservations.length && !this.props.errorMessage}>
                    <HelpMessage info>No reservations for this region</HelpMessage>
                </Conditional>

                <HelpMessage error>
                    {this.props.errorMessage}
                </HelpMessage>
            </div>
        );
    }
}
