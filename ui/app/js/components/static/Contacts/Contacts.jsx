import React from 'react';
import SectionTitle from "../../utils/SectionTitle/SectionTitle";


export default class Contacts extends React.Component {
    render() {
        return (
            <div className="content">
                <SectionTitle text="Contacts"/>

                <h1 className="has-text-light is-size-5">Stanislav Glubokiy</h1>
                <div><span className="has-text-weight-light">Skype:</span> <a href="skype:stas-deep?chat">stas-deep</a></div>
                <div><span className="has-text-weight-light">Github:</span> <a href="https://github.com/StasDeep">StasDeep</a></div>
                <div><span className="has-text-weight-light">Bitbucket:</span> <a href="https://bitbucket.org/StasDeep/">StasDeep</a></div>
                <h1 className="has-text-light is-size-5">Yuri Sagalovich</h1>
                <div><span className="has-text-weight-light">Skype:</span> <a href="skype:yuriy.sahalovich?chat">yuriy.sahalovich</a></div>
                <div><span className="has-text-weight-light">Github:</span> <a href="https://github.com/4YOUra">4YOUra</a></div>
                <div><span className="has-text-weight-light">Bitbucket:</span> <a href="https://bitbucket.org/4YOUra">4YOUra</a></div>
            </div>
        );
    }
}
