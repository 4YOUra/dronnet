export const NEW_DRONE_REQUEST = 'newDrone/REQUEST';
export const NEW_DRONE_RECEIVE = 'newDrone/RECEIVE';
export const NEW_DRONE_ERROR = 'newDrone/ERROR';

export const NEW_DRONE_REQUEST_DETAILS = 'newDrone/REQUEST_DETAILS';
export const NEW_DRONE_RECEIVE_DETAILS = 'newDrone/RECEIVE_DETAILS';
export const NEW_DRONE_ERROR_DETAILS = 'newDrone/ERROR_DETAILS';

export const NEW_DRONE_REQUEST_DELETE = 'newDrone/REQUEST_DELETE';
export const NEW_DRONE_RECEIVE_DELETE = 'newDrone/RECEIVE_DELETE';
export const NEW_DRONE_ERROR_DELETE = 'newDrone/ERROR_DELETE';

export const NEW_DRONE_SET_NAME = 'newDrone/SET_NAME';
export const NEW_DRONE_SET_MODEL = 'newDrone/SET_MODEL';
export const NEW_DRONE_RESET_FORM = 'newDrone/RESET_FORM';
