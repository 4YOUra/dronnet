import React from 'react';
import cn from 'classnames';
import SectionTitle from '../../utils/SectionTitle/SectionTitle';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import DroneModelBox from '../DroneModelBox/DroneModelBox';


export default class DroneModelsList extends React.Component {
    static propTypes = {
        droneModels: PropTypes.array.isRequired,
        errorMessage: PropTypes.string,
        isFetching: PropTypes.bool,
        onBoxClick: PropTypes.func.isRequired
    };

    static defaultProps = {
        errorMessage: '',
        isFetching: false,
    };

    render() {
        return (
            <div>
                <SectionTitle text="Drone Models" withSpinner={this.props.isFetching}>
                    <Link to="/admin/create-drone-model" className="button is-primary">
                        New
                    </Link>
                </SectionTitle>

                {this.props.droneModels.map((droneModel) =>
                    <DroneModelBox
                        {...droneModel}
                        key={droneModel.id}
                        onBoxClick={() => this.props.onBoxClick(droneModel)}
                    />
                )}

                {!this.props.isFetching && !this.props.droneModels.length && !this.props.errorMessage &&
                    <div>No drone models</div>
                }

                {this.props.errorMessage &&
                    <div className="is-size-6 has-text-danger">{this.props.errorMessage}</div>
                }
            </div>
        );
    }
}
