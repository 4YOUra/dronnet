import * as types from './types';
import {errorDispatchAndReject} from '../../utils/utils';
import * as authService from '../../services/auth';


export function signUpRequest() {
    return {
        type: types.SIGNUP_REQUEST
    }
}

export function signUpReceive() {
    return {
        type: types.SIGNUP_RECEIVE
    }
}

export function signUpError(message) {
    return {
        type: types.SIGNUP_ERROR,
        message
    }
}

export function signUpAsync({email, username, password}) {
    return (dispatch) => {
        dispatch(signUpRequest());

        return authService.signUpAsync(email, username, password).then(
            (response) => {
                dispatch(signUpReceive());
                return response;
            },
            errorDispatchAndReject(dispatch, signUpError, 'Unable to create new account')
        );
    };
}
