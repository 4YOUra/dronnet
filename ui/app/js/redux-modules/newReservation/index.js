import * as types from './types';


const initialState = {
    drones: [],
    startDate: null,
    endDate: null,
    height: 4,
    isFetching: false,
    isFetchingDrones: false,
    reservationIdToFilter: null,
};


export default function newReservation(state = initialState, action) {
    switch (action.type) {

        case types.NEW_RES_SET_START_DATE:
            return {
                ...state,
                startDate: action.date
            };

        case types.NEW_RES_SET_END_DATE:
            return {
                ...state,
                endDate: action.date
            };

        case types.NEW_RES_SET_HEIGHT:
            return {
                ...state,
                height: action.height
            };

        case types.NEW_RES_REQUEST:
            return {
                ...state,
                isFetching: true,
                errorMessage: '',
            };

        case types.NEW_RES_RECEIVE:
            return {
                ...initialState,
                isFetching: false,
            };

        case types.NEW_RES_ERROR:
            return {
                ...state,
                isFetching: false,
                errorMessage: action.message,
            };

        case types.NEW_RES_SET_FILTER_ID:
            return {
                ...state,
                reservationIdToFilter: action.id
            };

        case types.NEW_RES_DRONE_REQUEST:
            return {
                ...state,
                isFetchingDrones: true,
                drones: [],
            };

        case types.NEW_RES_DRONE_RECEIVE:
            return {
                ...state,
                isFetchingDrones: false,
                errorMessage: '',
                drones: action.items,
            };

        case types.NEW_RES_DRONE_ERROR:
            return {
                ...state,
                isFetchingDrones: false,
                errorMessage: action.message
            };

        case types.NEW_RES_DRONE_SELECT:
            return {
                ...state,
                drones: state.drones.map((d) => ({...d, isSelected: action.droneId === d.id ? true : d.isSelected}))
            };

        case types.NEW_RES_DRONE_DESELECT:
            return {
                ...state,
                drones: state.drones.map((d) => ({...d, isSelected: action.droneId === d.id ? false : d.isSelected}))
            };

        case types.NEW_RES_RESET_FORM:
            return initialState;

        default:
            return state
    }
}
