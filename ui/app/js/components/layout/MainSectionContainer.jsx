import {connect} from 'react-redux';
import React from "react";
import MainSection from './MainSection/MainSection';


@connect(
    (state) => ({
        isOpen: state.ui.isSectionOpen,
        location: state.router.location,
    })
)
export default class MainSectionContainer extends React.Component {
    render() {
        return (
            <MainSection
                {...this.props}
            />
        );
    }
}
