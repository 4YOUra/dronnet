import {connect} from 'react-redux';
import React from 'react';
import {push} from 'react-router-redux';
import {
    createDroneAsync, deleteDroneAsync, newDroneResetForm, newDroneSetModel, newDroneSetName,
    setDroneForEditAsync, updateDroneAsync
} from "../../redux-modules/newDrone/actions";
import {CREATE_MODE, UPDATE_MODE} from "../../utils/constants";
import {fetchDroneModelsAsync} from "../../redux-modules/droneModel/actions";
import DroneForm from "./DroneForm/DroneForm";


@connect(
    (state) => ({
        droneModels: state.droneModel.items,
        errorMessage: state.newDrone.errorMessage,
        isFetching: state.newDrone.isFetching,
        isFetchingDetails: state.newDrone.isFetchingDetails,
        isFetchingDelete: state.newDrone.isFetchingDelete,
        model: state.newDrone.model,
        name: state.newDrone.name,
    }),
    {
        createDroneAsync,
        deleteDroneAsync,
        fetchDroneModelsAsync,
        newDroneResetForm,
        setDroneForEditAsync,
        updateDroneAsync,
        changeRoute: push,
        onModelChange: newDroneSetModel,
        onNameChange: newDroneSetName,
    }
)
export default class DroneFormContainer extends React.Component {
    constructor(props) {
        super(props);
        this.editMode = this.props.match.params.droneId ? UPDATE_MODE : CREATE_MODE;
    }

    componentDidMount() {
        this.props.fetchDroneModelsAsync().then(
            (response) => {
                const droneModel = response.data[0];
                if (droneModel) {
                    this.props.onModelChange(droneModel.id);
                }
            }
        );

        if (this.editMode === UPDATE_MODE) {
            this.droneId = parseInt(this.props.match.params.droneId);
            this.props.setDroneForEditAsync(this.droneId);
        }
    }

    componentWillUnmount() {
        this.props.newDroneResetForm();
    }

    handleDeleteButtonClick = () => {
        this.props.deleteDroneAsync(this.droneId).then(
            () => this.props.changeRoute('/profile/drones')
        );
    };

    handleFormSubmit = () => {
        if (this.editMode === CREATE_MODE) {
            this.props.createDroneAsync().then(
                () => this.props.changeRoute('/profile/drones')
            );
        }

        if (this.editMode === UPDATE_MODE) {
            this.props.updateDroneAsync(this.droneId).then(
                () => this.props.changeRoute('/profile/drones')
            );
        }
    };

    render() {
        return (
            <DroneForm
                {...this.props}
                editMode={this.editMode}
                onDeleteButtonClick={this.handleDeleteButtonClick}
                onFormSubmit={this.handleFormSubmit}
            />
        )
    }
}
