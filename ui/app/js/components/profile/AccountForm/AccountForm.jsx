import React from 'react';
import PropTypes from 'prop-types';
import Field from "../../utils/Field/Field";
import Input from "../../utils/Input/Input";
import SubmitButton from "../../utils/SubmitButton/SubmitButton";
import HelpMessage from "../../utils/HelpMessage/HelpMessage";


export default class AccountForm extends React.Component {
    static propTypes = {
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        address: PropTypes.string.isRequired,
        phone: PropTypes.string.isRequired,
        timezone: PropTypes.string.isRequired,
        onFirstNameChange: PropTypes.func.isRequired,
        onLastNameChange: PropTypes.func.isRequired,
        onAddressChange: PropTypes.func.isRequired,
        onPhoneChange: PropTypes.func.isRequired,
        onTimezoneChange: PropTypes.func.isRequired,
        errorMessage: PropTypes.string,
        successMessage: PropTypes.string,
        isFetching: PropTypes.bool,
        onFormSubmit: PropTypes.func.isRequired,
    };

    static defaultProps = {
        errorMessage: '',
        successMessage: '',
        isFetching: false,
    };

    handleFormSubmit = (event) => {
        event.preventDefault();

        this.props.onFormSubmit();
    };

    render() {
        return (
            <form className="form is-dark" onSubmit={this.handleFormSubmit}>
                <Field label="First Name">
                    <Input
                        placeholder="John"
                        value={this.props.firstName}
                        onChange={(e) => this.props.onFirstNameChange(e.target.value)}
                    />
                </Field>

                <Field label="Last Name">
                    <Input
                        placeholder="Doe"
                        value={this.props.lastName}
                        onChange={(e) => this.props.onLastNameChange(e.target.value)}
                    />
                </Field>

                <Field label="Address">
                    <Input
                        placeholder="East str. 4"
                        value={this.props.address}
                        onChange={(e) => this.props.onAddressChange(e.target.value)}
                    />
                </Field>

                <Field label="Phone">
                    <Input
                        placeholder="+375291963433"
                        value={this.props.phone}
                        onChange={(e) => this.props.onPhoneChange(e.target.value)}
                    />
                </Field>

                <Field label="Timezone">
                    <Input
                        placeholder="Europe/Minsk"
                        value={this.props.timezone}
                        onChange={(e) => this.props.onTimezoneChange(e.target.value)}
                    />
                </Field>

                <SubmitButton isFetching={this.props.isFetching}>
                    Save
                </SubmitButton>

                <HelpMessage error>
                    {this.props.errorMessage}
                </HelpMessage>

                <HelpMessage color="success">
                    {this.props.successMessage}
                </HelpMessage>
            </form>
        );
    }
}
