import React from 'react';
import PropTypes from 'prop-types';
import SectionTitle from "../../utils/SectionTitle/SectionTitle";
import {Link} from "react-router-dom";
import DroneBox from "../DroneBox/DroneBox";
import HelpMessage from "../../utils/HelpMessage/HelpMessage";


export default class DronesList extends React.Component {
    static propTypes = {
        drones: PropTypes.array.isRequired,
        errorMessage: PropTypes.string,
        isFetching: PropTypes.bool,
        isFetchingModels: PropTypes.bool,
        onBoxClick: PropTypes.func.isRequired
    };

    static defaultProps = {
        errorMessage: '',
        isFetching: false,
        isFetchingModels: false,
    };

    render() {
        return (
            <div>
                <SectionTitle text="Drones" withSpinner={this.props.isFetching || this.props.isFetchingModels}>
                    <Link to="/profile/create-drone" className="button is-primary">
                        New
                    </Link>
                </SectionTitle>

                {this.props.drones.map((drone) =>
                    <DroneBox
                        {...drone}
                        key={drone.id}
                        onBoxClick={() => this.props.onBoxClick(drone)}
                    />
                )}

                {!this.props.isFetching && !this.props.drones.length && !this.props.errorMessage &&
                    <HelpMessage info>You don't have any drones</HelpMessage>
                }

                <HelpMessage error>
                    {this.props.errorMessage}
                </HelpMessage>
            </div>
        );
    }
}
