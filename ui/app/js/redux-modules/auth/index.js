import * as types from "./types";
import moment from 'moment-timezone';

const defaultTimezone = moment.tz.guess();

const initialState = {
    errorMessage: '',
    isFetching: false,
    user: localStorage.getItem('token') ? {
        type: 'C',
        timezone: defaultTimezone
    } : {
        anon: true,
        timezone: defaultTimezone
    },
};


export default function auth(state = initialState, action) {
    switch (action.type) {

        case types.AUTH_LOGIN_REQUEST:
            return {
                ...state,
                isFetching: true,
            };

        case types.AUTH_LOGIN_ERROR:
            return {
                ...state,
                isFetching: false,
                errorMessage: action.message
            };

        case types.AUTH_USER_RECEIVE:
            return {
                ...state,
                user: action.user
            };

        case types.AUTH_USER_ERROR:
            return {
                ...state,
                user: initialState.user,
            };

        default:
            return state
    }
}
