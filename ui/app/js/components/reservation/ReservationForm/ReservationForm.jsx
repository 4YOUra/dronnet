import React from 'react';
import PropTypes from 'prop-types';
import s from './style.scss';
import cn from 'classnames';
import HelpMessage from '../../utils/HelpMessage/HelpMessage';
import {CIRCLE, POLYGON} from "../../../redux-modules/draw/index";
import SubmitButton from '../../utils/SubmitButton/SubmitButton';
import Select from '../../utils/Select/Select';
import Field from "../../utils/Field/Field";
import Input from "../../utils/Input/Input";
import SectionTitle from "../../utils/SectionTitle/SectionTitle";
import {UPDATE_MODE} from "../../../utils/constants";
import Conditional from '../../utils/Conditional/Conditional';
import {Link} from 'react-router-dom';


export default class ReservationForm extends React.Component {
    static propTypes = {
        defaultStartDateString: PropTypes.string,
        defaultEndDateString: PropTypes.string,
        drawMode: PropTypes.number.isRequired,
        drawnCircle: PropTypes.object,
        drawnPolygon: PropTypes.object,
        drones: PropTypes.array.isRequired,
        editMode: PropTypes.string.isRequired,
        errorMessage: PropTypes.string,
        height: PropTypes.number.isRequired,
        isFetching: PropTypes.bool,
        isFetchingDrones: PropTypes.bool,
        isFetchingReservations: PropTypes.bool,
        onCircleResetClick: PropTypes.func.isRequired,
        onCircleRadiusChange: PropTypes.func.isRequired,
        onDrawModeChange: PropTypes.func.isRequired,
        onDroneClick: PropTypes.func.isRequired,
        onEndDateStringChange: PropTypes.func.isRequired,
        onFormSubmit: PropTypes.func.isRequired,
        onHeightChange: PropTypes.func.isRequired,
        onPolygonResetClick: PropTypes.func.isRequired,
        onStartDateStringChange: PropTypes.func.isRequired,
    };

    static defaultProps = {
        defaultStartDateString: '',
        defaultEndDateString: '',
        drawnCircle: {},
        drawnPolygon: {},
        errorMessage: '',
        isFetching: false,
        isFetchingDrones: false,
        isFetchingReservations: false,
    };

    handleFormSubmit = (event) => {
        event.preventDefault();

        this.props.onFormSubmit();
    };

    render() {
        return (
            <form className="form is-dark" onSubmit={this.handleFormSubmit}>
                <SectionTitle
                    text={this.props.editMode === UPDATE_MODE ? "Update Reservation" : "Create Reservation"}
                    withSpinner={this.props.isFetchingReservations || this.props.isFetchingDrones}
                />

                <Field label="Start Time">
                    <Input
                        placeholder="2018.03.13 17:00"
                        onChange={(e) => this.props.onStartDateStringChange(e.target.value)}
                        name="startDate"
                        defaultValue={this.props.defaultStartDateString}
                    />
                </Field>

                <Field label="End Time">
                    <Input
                        placeholder="2018.03.13 18:00"
                        onChange={(e) => this.props.onEndDateStringChange(e.target.value)}
                        name="endDate"
                        defaultValue={this.props.defaultEndDateString}
                    />
                </Field>

                <Field label="Drones">
                    {this.props.drones.map((drone) =>
                        <div className={cn(s.droneBoxContainer, {[s.droneSelected]: drone.isSelected})}
                             key={drone.id}
                             onClick={() => this.props.onDroneClick(drone)}>
                            <span className={s.droneBox}></span>
                            {drone.name}
                        </div>
                    )}

                    <Conditional if={!this.props.drones.length && !this.props.isFetchingDrones}>
                        <HelpMessage>
                            You don't have any drones, <Link to="/profile/create-drone">create one</Link>.
                        </HelpMessage>
                    </Conditional>
                </Field>

                <Field label="Height">
                    <Select
                        value={this.props.height}
                        onChange={(e) => this.props.onHeightChange(parseInt(e.target.value))}
                        choices={enums.reservationHeights.choices}
                    />
                </Field>

                <Field label="Shape">
                    <div className="buttons has-addons">
                        <button type="button"
                                className={cn("button is-dark", {'is-selected': this.props.drawMode === CIRCLE})}
                                onClick={() => this.props.onDrawModeChange(CIRCLE)}>
                            Circle
                        </button>
                        <button type="button"
                                className={cn("button is-dark", {'is-selected': this.props.drawMode === POLYGON})}
                                onClick={() => this.props.onDrawModeChange(POLYGON)}>
                            Polygon
                        </button>
                    </div>
                </Field>

                {this.props.drawMode === CIRCLE &&
                (this.props.drawnCircle.center.lat ?
                        <HelpMessage>
                            You can now drag, edit or
                            <a className="has-text-danger" onClick={this.props.onCircleResetClick}> reset </a>
                            the circle
                        </HelpMessage>
                        :
                        <HelpMessage>Choose center of the circle by clicking on the map</HelpMessage>
                )
                }

                {this.props.drawMode === POLYGON &&
                (this.props.drawnPolygon.coordinates.length ?
                        <HelpMessage>
                            You can now drag, edit or
                            <a className="has-text-danger" onClick={this.props.onPolygonResetClick}> reset </a>
                            the polygon
                        </HelpMessage>
                        :
                        <HelpMessage>Draw polygon zone by clicking on the map</HelpMessage>
                )
                }

                {this.props.drawMode === CIRCLE &&
                <Field label="Radius" subLabel="(meters)">
                    <Input
                        placeholder="20"
                        name="radius"
                        type="number"
                        min="1"
                        onChange={(e) => {
                            this.props.onCircleRadiusChange(parseInt(e.target.value))
                        }}
                        value={this.props.drawnCircle.radius}
                    />
                </Field>
                }

                <SubmitButton isFetching={this.props.isFetching}>
                    {this.props.editMode === UPDATE_MODE ? "Save" : "Create"}
                </SubmitButton>

                <HelpMessage error>
                    {this.props.errorMessage}
                </HelpMessage>
            </form>
        );
    }
}
