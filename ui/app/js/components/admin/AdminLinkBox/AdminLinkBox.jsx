import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';
import s from './style.scss';
import {Link} from "react-router-dom";


export default class AdminLinkBox extends React.Component {
    static propTypes = {
        to: PropTypes.string.isRequired,
    };

    render() {
        return (
            <div className="box">
                <div className="media">
                    <Link to={this.props.to}><h1 className={cn("title is-3", s.title)}>{this.props.children}</h1></Link>
                </div>
            </div>
        );
    }
}
