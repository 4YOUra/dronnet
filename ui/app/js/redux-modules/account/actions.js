import * as types from './types';
import {errorDispatchAndReject} from '../../utils/utils';
import * as userService from "../../services/user";


export function accountRequest() {
    return {
        type: types.ACCOUNT_REQUEST
    }
}

export function accountReceive() {
    return {
        type: types.ACCOUNT_RECEIVE
    }
}

export function accountError(message) {
    return {
        type: types.ACCOUNT_ERROR,
        message
    }
}

export function accountSetFirstName(firstName) {
	return {
		type: types.ACCOUNT_SET_FIRST_NAME,
		firstName
	}
}

export function accountSetLastName(lastName) {
	return {
		type: types.ACCOUNT_SET_LAST_NAME,
		lastName
	}
}

export function accountSetAddress(address) {
	return {
		type: types.ACCOUNT_SET_ADDRESS,
		address
	}
}

export function accountSetPhone(phone) {
	return {
		type: types.ACCOUNT_SET_PHONE,
		phone
	}
}

export function accountSetTimezone(timezone) {
	return {
		type: types.ACCOUNT_SET_TIMEZONE,
		timezone
	}
}

export function accountResetForm() {
    return {
        type: types.ACCOUNT_RESET_FORM
    }
}

export function setAccountForm() {
    return (dispatch, getState) => {
        const state = getState();

        dispatch(accountSetFirstName(state.auth.user.firstName));
        dispatch(accountSetLastName(state.auth.user.lastName));
        dispatch(accountSetAddress(state.auth.user.address));
        dispatch(accountSetPhone(state.auth.user.phone));
        dispatch(accountSetTimezone(state.auth.user.timezone));
    }
}

export function updateAccountAsync() {
    return (dispatch, getState) => {
        const state = getState();
        const firstName = state.account.firstName;
        const lastName = state.account.lastName;
        const address = state.account.address;
        const phone = state.account.phone;
        const timezone = state.account.timezone;

        dispatch(accountRequest());
        return userService.updateAccountAsync({firstName, lastName, address, phone, timezone}).then(
            (response) => {
                dispatch(accountReceive());
                return response;
            },
            errorDispatchAndReject(dispatch, accountError, 'Unable to update account')
        );
    };
}
