import pytest
from rest_framework import status


@pytest.mark.django_db
def test_create_drone(admin_client, valid_drone_data):
    response = admin_client.post('/api/drones/', valid_drone_data)
    assert response.status_code == status.HTTP_201_CREATED
