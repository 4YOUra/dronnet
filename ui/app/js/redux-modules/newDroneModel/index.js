import * as types from "./types";


const initialState = {
    errorMessage: '',
    isFetching: false,
    isFetchingDetails: false,
    isFetchingDelete: false,
    manufacturer: '',
    modelName: '',
};


export default function newDroneModel(state = initialState, action) {
    switch (action.type) {

        case types.NEW_DMODEL_REQUEST:
            return {
                ...state,
                isFetching: true
            };

        case types.NEW_DMODEL_RECEIVE:
            return {
                ...state,
                isFetching: false,
                errorMessage: ''
            };

        case types.NEW_DMODEL_ERROR:
            return {
                ...state,
                isFetching: false,
                errorMessage: action.message
            };

        case types.NEW_DMODEL_REQUEST_DETAILS:
            return {
                ...state,
                isFetchingDetails: true
            };

        case types.NEW_DMODEL_RECEIVE_DETAILS:
            return {
                ...state,
                isFetchingDetails: false,
                errorMessage: ''
            };

        case types.NEW_DMODEL_ERROR_DETAILS:
            return {
                ...state,
                isFetchingDetails: false,
                errorMessage: action.message
            };

        case types.NEW_DMODEL_REQUEST_DELETE:
            return {
                ...state,
                isFetchingDelete: true
            };

        case types.NEW_DMODEL_RECEIVE_DELETE:
            return {
                ...state,
                isFetchingDelete: false,
                errorMessage: ''
            };

        case types.NEW_DMODEL_ERROR_DELETE:
            return {
                ...state,
                isFetchingDelete: false,
                errorMessage: action.message
            };

        case types.NEW_DMODEL_SET_MANUFACTURER:
            return {
                ...state,
                manufacturer: action.manufacturer
            };

        case types.NEW_DMODEL_SET_MODEL_NAME:
            return {
                ...state,
                modelName: action.modelName
            };

        case types.NEW_DMODEL_RESET_FORM:
            return initialState;

        default:
            return state
    }
}
