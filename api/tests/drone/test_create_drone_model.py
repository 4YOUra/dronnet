import pytest
from rest_framework import status


@pytest.mark.django_db
def test_create_drone_model_by_admin(admin_client, valid_drone_model_data):
    response = admin_client.post('/api/drone-models/', valid_drone_model_data)
    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.django_db
def test_create_drone_model_by_regular_user(user_client, valid_drone_model_data):
    response = user_client.post('/api/drone-models/', valid_drone_model_data)
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_create_identical_drone_models(admin_client, valid_drone_model_data):
    response = admin_client.post('/api/drone-models/', valid_drone_model_data)
    assert response.status_code == status.HTTP_201_CREATED

    response = admin_client.post('/api/drone-models/', valid_drone_model_data)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
