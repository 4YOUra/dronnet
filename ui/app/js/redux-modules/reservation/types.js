export const RESERVATIONS_REQUEST = 'reservation/REQUEST';
export const RESERVATIONS_RECEIVE = 'reservation/RECEIVE';
export const RESERVATIONS_ERROR = 'reservation/ERROR';
export const RESERVATIONS_SET_DATE_FILTER = 'reservation/SET_DATE_FILTER';
export const RESERVATIONS_SET_FILTER_MODE = 'reservation/SET_FILTER_MODE';
export const RESERVATIONS_HOVER = 'reservation/HOVER';
