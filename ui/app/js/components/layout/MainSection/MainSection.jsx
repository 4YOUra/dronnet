import React from 'react';
import s from './style.scss';
import {Route, Switch} from 'react-router-dom';
import cn from 'classnames';
import LogInContainer from "../../auth/LogInContainer";
import PropTypes from 'prop-types';
import SignUpContainer from "../../auth/SignUpContainer";
import ReservationsListContainer from '../../reservation/ReservationsListContainer';
import ReservationFormContainer from "../../reservation/ReservationFormContainer";
import RouteAuthenticated from '../../utils/RouteAuthenticated/RouteAuthenticated';
import RouteAdmin from "../../utils/RouteAdmin/RouteAdmin";
import AdminPage from '../../admin/AdminPage/AdminPage';
import DroneModelsListContainer from '../../admin/DroneModelsListContainer';
import DroneModelFormContainer from "../../admin/DroneModelFormContainer";
import UsersListContainer from "../../admin/UsersListContainer";
import AccountFormContainer from "../../profile/AccountFormContainer";
import DronesListContainer from "../../drone/DronesListContainer";
import DroneFormContainer from "../../drone/DroneFormContainer";
import Contacts from "../../static/Contacts/Contacts";
import About from "../../static/About/About";


export default class MainSection extends React.Component {
    static propTypes = {
        isOpen: PropTypes.bool.isRequired,
        location: PropTypes.object.isRequired, // For re-render when path changes
    };

    render() {
        return (
            <div className={cn(s.mainSection, {[s.hidden]: !this.props.isOpen})}>
                <Switch location={this.props.location}>
                    <Route path='/login' component={LogInContainer}/>
                    <Route path='/signup' component={SignUpContainer}/>
                    <Route path='/contacts' component={Contacts}/>
                    <Route path='/about' component={About}/>

                    <RouteAuthenticated exact path='/' component={ReservationsListContainer}/>
                    <RouteAuthenticated path='/create-reservation' component={ReservationFormContainer}/>

                    <RouteAuthenticated path='/profile/account' component={AccountFormContainer}/>
                    <RouteAuthenticated path='/profile/drones' component={DronesListContainer}/>
                    <RouteAuthenticated path='/profile/create-drone' component={DroneFormContainer}/>
                    <RouteAuthenticated path='/profile/update-drone/:droneId' component={DroneFormContainer}/>
                    <RouteAuthenticated
                        path='/profile/reservations'
                        render={(props) => <ReservationsListContainer {...props} onlyOwn/>}
                    />

                    <RouteAdmin path='/update-reservation/:reservationId' component={ReservationFormContainer}/>
                    <RouteAdmin exact path='/admin' component={AdminPage}/>
                    <RouteAdmin path='/admin/drone-models' component={DroneModelsListContainer}/>
                    <RouteAdmin path='/admin/create-drone-model' component={DroneModelFormContainer}/>
                    <RouteAdmin path='/admin/update-drone-model/:droneModelId' component={DroneModelFormContainer}/>
                    <RouteAdmin path='/admin/users' component={UsersListContainer}/>
                </Switch>
            </div>
        );
    }
}
