import pytest

from rest_framework import status


@pytest.mark.django_db
def test_successful_retrieve(user_client, db_user, db_owner):
    response = user_client.get('/api/owners/')
    response_data = response.json()

    assert response.status_code == status.HTTP_200_OK
    assert response_data['username'] == db_user.username
    assert response_data['phone'] == db_owner.phone


@pytest.mark.django_db
def test_unsuccessful_retrieve(anon_client):
    response = anon_client.get('/api/owners/')
    response_data = response.json()

    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert 'detail' in response_data
    assert response_data['detail'] == 'Authentication credentials were not provided.'
