import React from 'react';
import PropTypes from 'prop-types';
import {CREATE_MODE, UPDATE_MODE} from "../../../utils/constants";
import SectionTitle from "../../utils/SectionTitle/SectionTitle";
import Field from "../../utils/Field/Field";
import Input from "../../utils/Input/Input";
import HelpMessage from "../../utils/HelpMessage/HelpMessage";
import Select from "../../utils/Select/Select";
import SubmitAndDeleteButtons from "../../utils/SubmitAndDeleteButtons/SubmitAndDeleteButtons";


export default class DroneForm extends React.Component {
    static propTypes = {
        droneModels: PropTypes.array.isRequired,
        editMode: PropTypes.string,
        errorMessage: PropTypes.string,
        isFetching: PropTypes.bool,
        isFetchingDetails: PropTypes.bool,
        isFetchingDelete: PropTypes.bool,
        isFetchingModels: PropTypes.bool,
        model: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        onFormSubmit: PropTypes.func.isRequired,
        onModelChange: PropTypes.func.isRequired,
        onNameChange: PropTypes.func.isRequired,
        onDeleteButtonClick: PropTypes.func.isRequired,
    };

    static defaultProps = {
        editMode: CREATE_MODE,
        errorMessage: '',
        isFetching: false,
        isFetchingDetails: false,
        isFetchingDelete: false,
        isFetchingModels: false,
    };

    handleFormSubmit = (event) => {
        event.preventDefault();

        this.props.onFormSubmit();
    };

    getDroneModelsChoices() {
        let choices = [];
        for (let droneModel of this.props.droneModels) {
            choices.push({value: droneModel.id, name: `${droneModel.manufacturer} ${droneModel.modelName}`});
        }
        return choices;
    }

    render() {
        return (
            <form className="form is-dark" onSubmit={this.handleFormSubmit}>
                <SectionTitle
                    text={this.props.editMode === UPDATE_MODE ? "Update Drone" : "Create Drone"}
                    withSpinner={this.props.isFetchingDetails || this.props.isFetchingModels}
                />

                <Field label="Name">
                    <Input
                        placeholder="My old DJI"
                        value={this.props.name}
                        onChange={(e) => this.props.onNameChange(e.target.value)}
                    />
                </Field>

                <Field label="Model">
                    <Select
                        value={this.props.model}
                        onChange={(e) => this.props.onModelChange(parseInt(e.target.value))}
                        choices={this.getDroneModelsChoices()}
                    />
                </Field>

                <SubmitAndDeleteButtons
                    editMode={this.props.editMode}
                    isFetching={this.props.isFetching}
                    isFetchingDelete={this.props.isFetchingDelete}
                    onDeleteButtonClick={this.props.onDeleteButtonClick}
                />

                <HelpMessage error>
                    {this.props.errorMessage}
                </HelpMessage>
            </form>
        );
    }
}
