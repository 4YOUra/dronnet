from django.conf.urls import url
from rest_framework.authtoken import views as rf_views

from api.auth.views import SignUpView

urlpatterns = [
    url(r'^login/$', rf_views.obtain_auth_token),
    url(r'^signup/$', SignUpView.as_view(), name='signup'),
]
