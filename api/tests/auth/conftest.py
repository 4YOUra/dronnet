import pytest

from api.tests.conftest import DEFAULT_USER_DATA

DEFAULT_SIGNUP_DATA = {
    'username': 'test_signup_user',
    'password': 'test_signup_password',
    'email': 'test_signup@test.com'
}


@pytest.fixture
def valid_signup_data():
    return DEFAULT_SIGNUP_DATA.copy()


@pytest.fixture
def invalid_email_data():
    data = DEFAULT_SIGNUP_DATA.copy()
    data['email'] = 'invalid email'
    return data


@pytest.fixture
def required_field_data(field):
    data = DEFAULT_SIGNUP_DATA.copy()
    data.pop(field)
    return data


@pytest.fixture
def valid_credentials():
    return DEFAULT_USER_DATA.copy()


@pytest.fixture
def invalid_credentials():
    data = DEFAULT_USER_DATA.copy()
    data['password'] = 'invalid password'
    return data
