import React from 'react';
import s from './style.scss';
import PropTypes from 'prop-types';
import cn from 'classnames';
import spinner from 'Images/dronnetspinner.png';


export default class Spinner extends React.Component {
    static propTypes = {
        small: PropTypes.bool,
        block: PropTypes.bool,
        className: PropTypes.string,
    };

    static defaultProps = {
        small: false,
        block: false,
        className: '',
    };

    render() {
        return (
            <span className={cn("has-text-centered", this.props.className,
                    s.spinnerContainer, {[s.spinnerBlock]: this.props.block})}>
                <img src={spinner} className={cn(s.spinner, {[s.medium]: !this.props.small, [s.small]: this.props.small})}/>
            </span>
        );
    }
}
