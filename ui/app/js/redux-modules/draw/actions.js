import * as types from "./types";


export function drawSetDrawMode(drawMode) {
    return {
        type: types.DRAW_SET_DRAW_MODE,
        drawMode
    }
}

export function drawSetRadius(radius) {
    return {
        type: types.DRAW_SET_RADIUS,
        radius
    }
}

export function drawSetCenter(center) {
    return {
        type: types.DRAW_SET_CENTER,
        center
    }
}

export function drawSetCoordinates(coordinates) {
    return {
        type: types.DRAW_SET_COORDINATES,
        coordinates
    }
}

export function drawResetCircle() {
    return {
        type: types.DRAW_RESET_CIRCLE
    }
}

export function drawResetPolygon() {
    return {
        type: types.DRAW_RESET_POLYGON
    }
}
