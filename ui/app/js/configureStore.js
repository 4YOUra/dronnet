import {applyMiddleware, createStore} from 'redux';
import reducer from './redux-modules/reducers';
import reduxThunk from 'redux-thunk';
import {routerMiddleware} from 'react-router-redux';


const logger = store => next => action => {
    if (!console.group) {
        return next(action);
    }

    console.group(action.type);
    console.log('%c prev state', 'color: gray', store.getState());
    console.log('%c action', 'color: #72a8ff', action);
    let result = next(action);
    console.log('%c next state', 'color: #71ffc8', store.getState());
    console.groupEnd(action.type);
    return result
};


export default function configureStore(history) {
    const createStoreWithMiddleware = applyMiddleware(reduxThunk, routerMiddleware(history), logger)(createStore);
    return createStoreWithMiddleware(reducer);
}
