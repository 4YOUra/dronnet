export const DRONE_REQUEST = 'drone/REQUEST';
export const DRONE_RECEIVE = 'drone/RECEIVE';
export const DRONE_ERROR = 'drone/ERROR';
