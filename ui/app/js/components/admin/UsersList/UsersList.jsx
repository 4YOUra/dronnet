import React from 'react';
import PropTypes from 'prop-types';
import SectionTitle from "../../utils/SectionTitle/SectionTitle";
import UserBox from "../UserBox/UserBox";


export default class UsersList extends React.Component {
    static propTypes = {
        users: PropTypes.array.isRequired,
        errorMessage: PropTypes.string,
        isFetching: PropTypes.bool
    };

    static defaultProps = {
        errorMessage: '',
        isFetching: false,
    };

    render() {
        return (
            <div>
                <SectionTitle text="Users" withSpinner={this.props.isFetching}/>

                {this.props.users.map((user) =>
                    <UserBox
                        {...user}
                        key={user.id}
                    />
                )}

                {!this.props.isFetching && !this.props.users.length && !this.props.errorMessage &&
                    <div>No users</div>
                }

                {this.props.errorMessage &&
                    <div className="is-size-6 has-text-danger">{this.props.errorMessage}</div>
                }
            </div>
        );
    }
}
