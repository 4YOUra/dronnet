import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import s from './style.scss';


export default class UserBox extends React.Component {
    static propTypes = {
        id: PropTypes.number.isRequired,
        username: PropTypes.string.isRequired,
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        email: PropTypes.string.isRequired,
        phone: PropTypes.string.isRequired,
        address: PropTypes.string.isRequired,
        type: PropTypes.string.isRequired,
        timezone: PropTypes.string.isRequired,
    };

    constructor(props) {
        super(props);

        this.state = {
            isOpen: false
        };
    }

    handleClick = () => {
        this.setState({isOpen: !this.state.isOpen});
    };

    render() {
        return (
            <div className={cn("box", s.box, {[s.isOpen]: this.state.isOpen})} onClick={this.handleClick}>
                <p>
                    <span className="has-text-weight-light">#{this.props.id}</span>
                    <span className="has-text-weight-bold is-uppercase"> {this.props.username}</span>
                </p>
                <div className={s.params}>
                    <p><span className="param-name">First Name:</span> {this.props.firstName || '-'}</p>
                    <p><span className="param-name">Last Name:</span> {this.props.lastName || '-'}</p>
                    <p><span className="param-name">Email:</span> {this.props.email}</p>
                    <p><span className="param-name">Phone:</span> {this.props.phone || '-'}</p>
                    <p><span className="param-name">Address:</span> {this.props.address || '-'}</p>
                    <p><span className="param-name">Type:</span> {enums.ownerTypes.labels[this.props.type]}</p>
                    <p><span className="param-name">Timezone:</span> {this.props.timezone}</p>
                </div>
            </div>
        );
    }
}
