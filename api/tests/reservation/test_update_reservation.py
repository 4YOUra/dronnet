import pytest
from rest_framework import status

from api.reservation.models import Reservation


@pytest.mark.django_db
def test_successful_update_reservation(admin_client, db_user_reservation):
    url = '/api/reservations/{}/'.format(db_user_reservation.id)
    response = admin_client.patch(url, {'height': Reservation.Heights.MEDIUM})
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_unsuccessful_update_reservation_circle(user_client, db_user_reservation):
    url = '/api/reservations/{}/'.format(db_user_reservation.id)
    response = user_client.patch(url, {'height': Reservation.Heights.MEDIUM})
    assert response.status_code == status.HTTP_403_FORBIDDEN
