import * as types from './types';
import {loadMapPositionFromLocalStorage} from "../../services/map";

const mapPosition = loadMapPositionFromLocalStorage() || {};

const initialState = {
    defaultCenter: mapPosition.center,
    defaultZoom: mapPosition.zoom,
    center: null,
    zoom: null,
    bounds: null,
    mapType: localStorage.getItem('mapType') || 'roadmap',
    currentBounds: mapPosition.bounds
};


export default function map(state = initialState, action) {
    switch (action.type) {

        case types.MAP_SET_CENTER:
            return {
                ...state,
                center: action.center
            };

        case types.MAP_SET_ZOOM:
            return {
                ...state,
                zoomObj: action.zoomObj
            };

        case types.MAP_SET_BOUNDS:
            return {
                ...state,
                bounds: action.bounds
            };


        case types.MAP_SET_CURRENT_BOUNDS:
            return {
                ...state,
                currentBounds: action.bounds
            };


        case types.MAP_SET_TYPE:
            return {
                ...state,
                mapType: action.mapType
            };

        default:
            return state
    }
}
