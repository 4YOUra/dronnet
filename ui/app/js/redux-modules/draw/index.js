import * as types from './types';
import * as newResTypes from "../newReservation/types";


export const NODRAW = 0;
export const CIRCLE = 1;
export const POLYGON = 2;

const initialState = {
    drawMode: NODRAW,
    circle: {
        radius: 100,
        center: {}
    },
    polygon: {
        coordinates: []
    }
};


export default function draw(state = initialState, action) {
    switch (action.type) {

        case types.DRAW_SET_DRAW_MODE:
            return {
                ...state,
                drawMode: action.drawMode
            };

        case types.DRAW_SET_RADIUS:
            return {
                ...state,
                circle: {
                    ...state.circle,
                    radius: action.radius
                }
            };

        case types.DRAW_SET_CENTER:
            return {
                ...state,
                circle: {
                    ...state.circle,
                    center: action.center
                }
            };

        case types.DRAW_SET_COORDINATES:
            return {
                ...state,
                polygon: {
                    ...state.polygon,
                    coordinates: action.coordinates
                }
            };

        case types.DRAW_RESET_CIRCLE:
            return {
                ...state,
                circle: initialState.circle
            };

        case types.DRAW_RESET_POLYGON:
            return {
                ...state,
                polygon: initialState.polygon
            };

        case newResTypes.NEW_RES_RESET_FORM:
            return initialState;

        default:
            return state;
    }
}
