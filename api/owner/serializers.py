from rest_framework import serializers

from api.owner.models import DroneOwnerProfile


class DroneOwnerProfileSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(source='user.first_name', allow_blank=True)
    last_name = serializers.CharField(source='user.last_name', allow_blank=True)
    email = serializers.CharField(source='user.email')
    username = serializers.CharField(source='user.username')

    class Meta:
        model = DroneOwnerProfile
        exclude = ('user', 'created_at')
        read_only_fields = ('email', 'username', 'type')

    def update(self, instance, validated_data):
        user = validated_data.pop('user', {})
        for field, value in user.items():
            if field not in self.Meta.read_only_fields:
                setattr(instance.user, field, value)
        instance.user.save()
        return super(DroneOwnerProfileSerializer, self).update(instance, validated_data)
