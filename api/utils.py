import math

from django.http import JsonResponse


EARTH_RADIUS = 6378.137


def get_distance_by_coords(lat1, lon1, lat2, lon2):
    dlat = (lat2 - lat1) * math.pi / 180
    dlon = (lon2 - lon1) * math.pi / 180
    a = (
        math.sin(dlat / 2) * math.sin(dlat / 2) +
        math.cos(lat1 * math.pi / 180) * math.cos(lat2 * math.pi / 180) *
        math.sin(dlon / 2) * math.sin(dlon / 2)
    )
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = EARTH_RADIUS * c
    return d * 1000


def coordinates_to_lnglat(coordinates):
    return [[coordinate[1], coordinate[0]] for coordinate in coordinates]


def coordinates_lnglat_to_dict(coordinates):
    return [{'lng': coordinate[0], 'lat': coordinate[1]} for coordinate in coordinates]


def get_circle_coordinates(center, radius):
    # Convert radius to km.
    radius /= 1000

    lat1 = math.radians(center[1])
    lon1 = math.radians(center[0])

    coordinates = []

    for b in range(0, 360, 10):
        brng = math.radians(b)

        lat2 = math.asin(math.sin(lat1) * math.cos(radius / EARTH_RADIUS) +
                         math.cos(lat1) * math.sin(radius / EARTH_RADIUS) * math.cos(brng))

        lon2 = lon1 + math.atan2(math.sin(brng) * math.sin(radius / EARTH_RADIUS) * math.cos(lat1),
                                 math.cos(radius / EARTH_RADIUS) - math.sin(lat1) * math.sin(lat2))

        coordinates.append([math.degrees(lon2), math.degrees(lat2)])

    return coordinates


class MetaEnum(type):
    def __getattribute__(cls, key):
        value = super(MetaEnum, cls).__getattribute__(key)

        if isinstance(value, tuple):
            return value[0]

        return value


class Enum(metaclass=MetaEnum):
    @classmethod
    def keys(cls):
        keys = [key for key in cls.__dict__.keys() if not key.startswith('__') and key.isupper()]
        keys.sort(key=lambda x: cls.value(x))
        return keys

    @classmethod
    def choices(cls):
        return [(cls.value(key), key) for key in cls.keys()]

    @classmethod
    def value(cls, key):
        return getattr(cls, key)

    @classmethod
    def label(cls, key):
        if isinstance(cls.__dict__[key], tuple):
            return cls.__dict__[key][1]

        return key.lower().capitalize()

    @classmethod
    def mapping(cls):
        return [(cls.value(key), key, cls.label(key)) for key in cls.keys()]

    @classmethod
    def as_view(cls):
        mapping = cls.mapping()

        def view(request):
            return JsonResponse(mapping, safe=False)

        return view
