from djangorestframework_camel_case.util import underscoreize
from rest_framework import generics
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated, IsAdminUser

from api.drone.models import Drone, DroneModel
from api.drone.serializers import DroneSerializer, DroneModelSerializer
from api.owner.models import DroneOwnerProfile


class DroneView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = DroneSerializer

    def get_queryset(self):
        params = underscoreize(self.request.query_params)
        user_id = params.get('user_id')

        if user_id:
            if self.request.user.profile.type == DroneOwnerProfile.Types.ADMIN:
                return Drone.objects.filter(owner__user=user_id)
            else:
                raise ValidationError('user_id is allowed only for admins')
        else:
            return Drone.objects.filter(owner__user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user.profile)


class DroneDetailsView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = DroneSerializer
    queryset = Drone.objects.all()


class DroneModelView(generics.ListCreateAPIView):
    serializer_class = DroneModelSerializer
    queryset = DroneModel.objects.all()

    def get_permissions(self):
        if self.request.method == 'GET':
            self.permission_classes = (IsAuthenticated,)
        else:
            self.permission_classes = (IsAdminUser,)

        return super(DroneModelView, self).get_permissions()


class DroneModelDetailsView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = DroneModelSerializer
    queryset = DroneModel.objects.all()

    def get_permissions(self):
        if self.request.method == 'GET':
            self.permission_classes = (IsAuthenticated,)
        else:
            self.permission_classes = (IsAdminUser,)

        return super(DroneModelDetailsView, self).get_permissions()
