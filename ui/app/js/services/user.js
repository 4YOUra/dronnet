import axios from 'axios';
import {apiUrl} from '../redux-modules/config';

export function fetchUserAsync() {
    return axios.get(`${apiUrl}/owners/`);
}

export function fetchUsersAsync() {
    return axios.get(`${apiUrl}/owners/all/`);
}

export function updateAccountAsync(data) {
    return axios.patch(`${apiUrl}/owners/`, data);
}
