import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import account from './account';
import auth from './auth';
import draw from './draw';
import drone from './drone';
import droneModel from './droneModel';
import map from './map';
import newDrone from './newDrone';
import newDroneModel from './newDroneModel';
import newReservation from './newReservation';
import signUp from './signUp';
import reservation from './reservation';
import ui from './ui';
import user from './user';
 
export default combineReducers({
    account,
    auth,
    draw,
    drone,
    droneModel,
    map,
    newDrone,
    newDroneModel,
    newReservation,
    signUp,
    reservation,
    ui,
    user,

    router: routerReducer
});
