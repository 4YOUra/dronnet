export function loadMapPositionFromLocalStorage() {
    const serializedPosition = localStorage.getItem('mapPosition');

    if (!serializedPosition) {
        return;
    }

    return JSON.parse(serializedPosition);
}

export function saveMapPositionToLocalStorage(center, zoom, bounds) {
    const serializedPosition = JSON.stringify({center, zoom, bounds});

    localStorage.setItem('mapPosition', serializedPosition);
}
