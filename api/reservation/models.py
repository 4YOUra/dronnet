from datetime import timedelta

from django.contrib.gis.db.models import Q
from django.contrib.gis.geos import Polygon
from django.contrib.gis.db import models

from api.drone.models import Drone
from api.owner.models import DroneOwnerProfile
from api.utils import Enum


class ReservationManager(models.Manager):
    def get_traffic(self, west, north, east, south, start_date, end_date, **kwargs):
        if west < east:
            # Polygon filter is a simple rectangle.
            polygon_filter = Polygon((
                (west, south),
                (west, north),
                (east, north),
                (east, south),
                (west, south)
            ))
        else:
            # If lon A > lonB it means that international date line is inside the region.
            # Polygon filter now consists of two polygons.
            polygon_filter = Polygon((
                (west, north),
                (west, south),
                (180, south),
                (180, north),
                (-180, north),
                (-180, south),
                (east, south),
                (east, north),
                (west, north)
            ))

        return self.get_intersections(start_date, end_date, polygon_filter, **kwargs)

    def get_intersections(self, start_date, end_date, zone, height=None, except_id=None):
        height = height or Reservation.Heights.VARIABLE

        # Shrink interval by 2 milliseconds to avoid edge-intersecting ranges.
        start_date = start_date + timedelta(milliseconds=1)
        end_date = end_date - timedelta(milliseconds=1)

        interval = (start_date, end_date)

        if height != Reservation.Heights.VARIABLE:
            height_intersectors = (height,)
        else:
            height_intersectors = (
                Reservation.Heights.LOW,
                Reservation.Heights.MEDIUM,
                Reservation.Heights.HIGH
            )

        return self.filter(
            Q(start_date__range=interval)
            | Q(end_date__range=interval)
            | Q(start_date__lte=start_date, end_date__gte=end_date),
            Q(height=4)
            | Q(height__in=height_intersectors),
            ~Q(id=except_id),
            zone__intersects=zone,
        )


class Reservation(models.Model):
    class Heights(Enum):
        LOW = 1, 'Low (to 50m)'
        MEDIUM = 2, 'Medium (from 50m to 200m)'
        HIGH = 3, 'High (from 200m)'
        VARIABLE = 4

    class Shape(Enum):
        POLYGON = 1
        CIRCLE = 2

    zone = models.PolygonField()
    owner = models.ForeignKey(DroneOwnerProfile, related_name='reservations')
    drones = models.ManyToManyField(Drone)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    height = models.IntegerField(choices=Heights.choices(), default=Heights.VARIABLE)
    shape = models.IntegerField(choices=Shape.choices(), default=Shape.POLYGON)

    objects = ReservationManager()

    class Meta:
        ordering = ('start_date',)
