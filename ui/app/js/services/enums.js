import axios from 'axios';
import {apiUrl} from '../redux-modules/config';


window.enums = {
    reservationHeights: {
        path: 'reservation-heights',
        fillColors: {
            LOW: '#03dc37',
            MEDIUM: '#00a8af',
            HIGH: '#0053cd',
            VARIABLE: '#171158',
        },
        strokeColors: {
            LOW: '#03902c',
            MEDIUM: '#00a8af',
            HIGH: '#0053cd',
            VARIABLE: '#7680ba',
        },
        shortLabels: {
            LOW: 'Low',
            MEDIUM: 'Medium',
            HIGH: 'High',
            VARIABLE: 'Variable',
        },
    },
    ownerTypes: 'owner-types',
};


class Enum {
    constructor(values, options) {
        this.labels = {};
        this.choices = [];

        for (let val of values) {
            this[val[1]] = val[0];
            this.labels[val[0]] = val[2];
            this.choices.push({value: val[0], name: val[2]});
        }

        for (let opt in options) {
            if (!options.hasOwnProperty(opt)) continue;

            this[opt] = {};

            for (let key in options[opt]) {
                if (!options[opt].hasOwnProperty(key)) continue;

                const val = this[key];

                if (typeof val === 'undefined') {
                    throw `Unknown property on ${opt}: ${key}`
                }

                this[opt][this[key]] = options[opt][key];
            }
        }
    }
}

export function fetchEnums() {
    let promises = [];

    for (const key in enums) {
        if (!enums.hasOwnProperty(key)) continue;

        let path = enums[key];
        let options = {};

        if (typeof path === 'object') {
            path = enums[key].path;
            delete enums[key].path;
            options = enums[key];
        }

        let pr = axios.get(`${apiUrl}/${path}/`).then((response) => enums[key] = new Enum(response.data, options));
        promises.push(pr);
    }

    return Promise.all(promises);
}
