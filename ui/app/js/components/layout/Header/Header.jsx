import React from 'react';
import s from './style.scss';
import logo from 'Images/dronnetlogo.png';
import logoSmall from 'Images/dronnetfavicon.png';
import {Link} from 'react-router-dom';
import HeaderLink from '../../utils/HeaderLink/HeaderLink';
import cn from 'classnames';
import PropTypes from 'prop-types';
import Clock from '../../utils/Clock/Clock';
import roadmapIcon from 'Images/dronnetmapicon.png';
import satelliteIcon from 'Images/dronnetsaticon.png';
import Conditional from '../../utils/Conditional/Conditional';


export default class Header extends React.Component {
    static propTypes = {
        isBurgerActive: PropTypes.bool.isRequired,
        mapType: PropTypes.string,
        onBurgerClick: PropTypes.func.isRequired,
        onLogOutClick: PropTypes.func.isRequired,
        onMapTypeClick: PropTypes.func.isRequired,
        path: PropTypes.string,
        user: PropTypes.object.isRequired,
    };

    static defaultProps = {
        mapType: 'roadmap',
        path: '/',
    };

    render() {
        return (
            <nav className="navbar is-black">
                <div className={cn('navbar-burger', s.burger, {'is-active': this.props.isBurgerActive})}
                     onClick={this.props.onBurgerClick}>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>

                <div className={cn("navbar-brand", s.hiddenTablet)}>
                    <Link to="/" className="navbar-item">
                        <img className={s.noSelection} src={logo} role="presentation"/>
                    </Link>
                </div>

                <div className={cn("navbar-brand", s.visibleTablet)}>
                    <Link to="/" className="navbar-item">
                        <img className={s.noSelection} src={logoSmall} role="presentation"/>
                    </Link>
                </div>

                <div className={cn("navbar-menu")}>
                    <div className={cn("navbar-start", s.hiddenMobile)}>
                        <Conditional if={!this.props.user.anon}>
                            <HeaderLink
                                to="/"
                                exact
                                className="navbar-item"
                                label="Reservations"
                            />
                        </Conditional>

                        <HeaderLink
                            to="/contacts"
                            className="navbar-item"
                            label="Contacts"
                        />

                        <HeaderLink
                            to="/about"
                            className="navbar-item"
                            label="About"
                        />
                    </div>

                    <div className={cn("navbar-end", s.hiddenMobile)}>
                        <a href="javascript:void(0);" className={cn("navbar-item", s.disabled, s.hiddenTablet)}>
                            <Clock/>
                        </a>

                        <a
                                href="javascript:void(0);"
                                className="navbar-item"
                                onClick={this.props.onMapTypeClick}>
                            {this.props.mapType === 'roadmap' ?
                                <img src={roadmapIcon} className={s.icon}/>
                                :
                                <img src={satelliteIcon} className={s.icon}/>
                            }
                        </a>

                        <Conditional if={this.props.user.type === enums.ownerTypes.ADMIN}>
                            <HeaderLink
                                to="/admin"
                                label="Admin"
                            />
                        </Conditional>

                        <Conditional if={this.props.user.anon}>
                            <HeaderLink
                                to="/login"
                                label="Log In"
                            />
                        </Conditional>

                        <Conditional if={this.props.user.anon}>
                            <HeaderLink
                                to="/signup"
                                label="Sign Up"
                            />
                        </Conditional>

                        <Conditional if={!this.props.user.anon}>
                            <div className={cn("navbar-item has-dropdown is-hoverable",
                                           {'is-active-dropdown': this.props.path.startsWith('/profile')})}>
                                <a className="navbar-link" href="javascript:void(0)">
                                    Profile
                                </a>
                                <div className="navbar-dropdown is-boxed is-dark is-shifted-left">
                                    <HeaderLink
                                        to="/profile/reservations"
                                        label="Reservations"
                                    />
                                    <HeaderLink
                                        to="/profile/drones"
                                        label="Drones"
                                    />
                                    <HeaderLink
                                        to="/profile/account"
                                        label="Account"
                                    />
                                    <hr className="navbar-divider"/>
                                    <a href="javascript:void(0);"
                                       className="navbar-item"
                                       onClick={this.props.onLogOutClick}>
                                        Log Out
                                    </a>
                                </div>
                            </div>
                        </Conditional>
                    </div>

                    <div className={cn("navbar-end", s.visibleMobileOnly)}>
                        <div className={cn("navbar-item has-dropdown is-hoverable")}>
                            <a className="navbar-link" href="javascript:void(0)">
                                Menu
                            </a>
                            <div className="navbar-dropdown is-boxed is-dark is-shifted-left">
                                <Conditional if={!this.props.user.anon}>
                                    <HeaderLink
                                        to="/"
                                        exact
                                        className="navbar-item"
                                        label="Reservations"
                                    />
                                </Conditional>

                                <HeaderLink
                                    to="/contacts"
                                    className="navbar-item"
                                    label="Contacts"
                                />

                                <HeaderLink
                                    to="/contacts"
                                    className="navbar-item"
                                    label="About"
                                />

                                <hr className="navbar-divider"/>

                                <Conditional if={this.props.user.type === enums.ownerTypes.ADMIN}>
                                    <HeaderLink
                                        to="/admin"
                                        label="Admin"
                                    />
                                </Conditional>

                                <Conditional if={this.props.user.anon}>
                                    <HeaderLink
                                        to="/login"
                                        label="Log In"
                                    />
                                </Conditional>

                                <Conditional if={this.props.user.anon}>
                                    <HeaderLink
                                        to="/signup"
                                        label="Sign Up"
                                    />
                                </Conditional>

                                <a href="javascript:void(0);"
                                   className="navbar-item"
                                   onClick={this.props.onMapTypeClick}>Toggle Map Type</a>

                                <Conditional if={!this.props.user.anon}>
                                    <hr className="navbar-divider"/>
                                </Conditional>

                                <Conditional if={!this.props.user.anon}>
                                    <HeaderLink
                                        to="/profile/reservations"
                                        label="My Reservations"
                                    />
                                </Conditional>

                                <Conditional if={!this.props.user.anon}>
                                    <HeaderLink
                                        to="/profile/drones"
                                        label="My Drones"
                                    />
                                </Conditional>

                                <Conditional if={!this.props.user.anon}>
                                    <HeaderLink
                                        to="/profile/account"
                                        label="My Account"
                                    />
                                </Conditional>

                                <Conditional if={!this.props.user.anon}>
                                    <hr className="navbar-divider"/>
                                </Conditional>

                                <Conditional if={!this.props.user.anon}>
                                    <a href="javascript:void(0);"
                                       className="navbar-item"
                                       onClick={this.props.onLogOutClick}>
                                        Log Out
                                    </a>
                                </Conditional>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}
