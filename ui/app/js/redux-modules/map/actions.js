import * as types from './types';


export function mapChangeCenter(center) {
    return {
        type: types.MAP_SET_CENTER,
        center: {
            lat: center.lat,
            lng: center.lng
        }
    }
}

export function mapChangeZoom(zoom) {
    return {
        type: types.MAP_SET_ZOOM,
        zoomObj: {
            value: zoom
        }
    }
}

export function mapChangeBounds(bounds) {
    return {
        type: types.MAP_SET_BOUNDS,
        bounds
    }
}

export function mapSaveBounds(bounds) {
    return {
        type: types.MAP_SET_CURRENT_BOUNDS,
        bounds
    }
}

export function mapSetType(mapType) {
    return {
        type: types.MAP_SET_TYPE,
        mapType
    }
}

export function resetCenterAndZoom() {
    return (dispatch) => {
        dispatch(mapChangeCenter({lat: 0, lng: 0}));
        dispatch(mapChangeZoom(2));
    };
}
