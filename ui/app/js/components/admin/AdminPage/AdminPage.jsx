import React from 'react';
import AdminLinkBox from "../AdminLinkBox/AdminLinkBox";


export default class AdminPage extends React.Component {
    render() {
        return (
            <div>
                <AdminLinkBox to="/">Reservations</AdminLinkBox>
                <AdminLinkBox to="/admin/drone-models">Drone Models</AdminLinkBox>
                <AdminLinkBox to="/admin/users">Users</AdminLinkBox>
            </div>
        );
    }
}
