import * as types from "./types";


const initialState = {
    isSectionOpen: true,
    isLegendOpen: !!localStorage.getItem('isLegendOpen'),
};


export default function ui(state = initialState, action) {
    switch (action.type) {

        case types.UI_OPEN_SECTION:
            return {
                ...state,
                isSectionOpen: true
            };

        case types.UI_CLOSE_SECTION:
            return {
                ...state,
                isSectionOpen: false
            };

        case types.UI_OPEN_LEGEND:
            return {
                ...state,
                isLegendOpen: true
            };

        case types.UI_CLOSE_LEGEND:
            return {
                ...state,
                isLegendOpen: false
            };

        default:
            return state;
    }
}
