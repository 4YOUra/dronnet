import React from 'react';
import PropTypes from 'prop-types';


export default class Select extends React.Component {
    static propTypes = {
        onChange: PropTypes.func,
        value: PropTypes.any,
        choices: PropTypes.array.isRequired,
    };

    static defaultProps = {
        onChange: () => {},
    };

    render() {
        return (
            <div className="select">
                <select value={this.props.value} onChange={this.props.onChange}>
                    {
                        (this.props.choices).map((choice) =>
                            <option value={choice.value} key={choice.value}>{choice.name}</option>
                        )
                    }
                </select>
            </div>
        );
    }
}
