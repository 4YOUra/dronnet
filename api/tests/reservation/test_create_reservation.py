import pytest
from rest_framework import status


@pytest.mark.django_db
def test_create_reservation_circle(user_client, valid_circle_res_data):
    response = user_client.post('/api/reservations/', valid_circle_res_data)
    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.django_db
def test_create_reservation_polygon(user_client, valid_polygon_res_data):
    response = user_client.post('/api/reservations/', valid_polygon_res_data)
    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.django_db
def test_create_reservation_without_radius(user_client, invalid_res_data_without_radius):
    response = user_client.post('/api/reservations/', invalid_res_data_without_radius)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_create_reservation_too_many_coords(user_client, invalid_res_data_too_many_coords):
    response = user_client.post('/api/reservations/', invalid_res_data_too_many_coords)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_create_reservation_coords_format(user_client, invalid_res_data_coords_format):
    response = user_client.post('/api/reservations/', invalid_res_data_coords_format)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_create_reservation_coords_value(user_client, invalid_res_data_coords_value):
    response = user_client.post('/api/reservations/', invalid_res_data_coords_value)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_create_reservation_no_coords(user_client, invalid_res_data_no_coords):
    response = user_client.post('/api/reservations/', invalid_res_data_no_coords)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_create_reservation_too_little_coords(user_client, invalid_res_data_too_little_coords):
    response = user_client.post('/api/reservations/', invalid_res_data_too_little_coords)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_create_reservation_in_the_past(user_client, invalid_res_data_in_the_past):
    response = user_client.post('/api/reservations/', invalid_res_data_in_the_past)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_create_reservation_too_long(user_client, invalid_res_data_too_long):
    response = user_client.post('/api/reservations/', invalid_res_data_too_long)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_create_reservation_end_prior_to_start(user_client, invalid_res_data_end_prior_to_start):
    response = user_client.post('/api/reservations/', invalid_res_data_end_prior_to_start)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_create_reservation_too_large(user_client, invalid_res_data_too_large):
    response = user_client.post('/api/reservations/', invalid_res_data_too_large)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_create_reservation_no_drones(user_client, invalid_res_data_no_drones):
    response = user_client.post('/api/reservations/', invalid_res_data_no_drones)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_create_reservation_wrong_drone(user_client, invalid_res_data_wrong_drone):
    response = user_client.post('/api/reservations/', invalid_res_data_wrong_drone)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_create_intersecting_reservations(user_client, valid_circle_res_data):
    response = user_client.post('/api/reservations/', valid_circle_res_data)
    assert response.status_code == status.HTTP_201_CREATED

    response = user_client.post('/api/reservations/', valid_circle_res_data)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_create_reservation_with_someones_drone(admin_client, db_admin_owner, valid_circle_res_data):
    # valid_circle_res_data uses regular user's drone.
    response = admin_client.post('/api/reservations/', valid_circle_res_data)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
