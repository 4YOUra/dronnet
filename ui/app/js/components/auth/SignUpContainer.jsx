import {connect} from 'react-redux';
import React from "react";
import SignUp from "./SignUp/SignUp";
import {signUpAsync} from "../../redux-modules/signUp/actions";
import {push} from 'react-router-redux';


@connect(
    (state) => ({
        isFetching: state.signUp.isFetching,
        errorMessage: state.signUp.errorMessage
    }),
    { signUpAsync, changeRoute: push }
)
export default class SignUpContainer extends React.Component {
    handleFormSubmit = (formData) => {
        this.props.signUpAsync(formData).then(
            () => this.props.changeRoute('/login')
        );
    };

    render() {
        return (
            <SignUp
                {...this.props}
                onFormSubmit={this.handleFormSubmit}
            />
        );
    }
}
