import React from 'react';
import PropTypes from 'prop-types';

export default class DateSelector extends React.Component {
    static propTypes = {
        defaultDate: PropTypes.object.isRequired,
        onDateChange: PropTypes.func,
    };

    static defaultProps = {
        onDateChange: () => {},
    };

    constructor(props) {
        super(props);

        this.state = {date: this.props.defaultDate};
    }

    getDateLabel() {
        return this.state.date.format('MMM D');
    }

    onAdd = () => {
        this.setNewDate(this.state.date.add(1, 'days'));
    };

    onSubtract = () => {
        this.setNewDate(this.state.date.subtract(1, 'days'));
    };

    setNewDate(newDate) {
        this.setState({date: newDate}, () => this.props.onDateChange(this.state.date));
    };

    render() {
        return (
            <div className="buttons has-addons">
                <span className="button is-small is-dark" onClick={this.onSubtract}>◂</span>
                <span className="button is-small is-dark">{this.getDateLabel()}</span>
                <span className="button is-small is-dark" onClick={this.onAdd}>▸</span>
            </div>
        );
    }
}
