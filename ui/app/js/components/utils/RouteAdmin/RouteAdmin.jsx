import React from 'react';
import {connect} from 'react-redux';
import {Redirect, Route} from 'react-router-dom';


@connect(
    (state) => ({
        user: state.auth.user
    })
)
export default class RouteAdmin extends React.Component {
    render() {
        return this.props.user.type !== enums.ownerTypes.ADMIN ? <Redirect to="/"/> : <Route {...this.props}/>
    }
}
