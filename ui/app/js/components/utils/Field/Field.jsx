import React from 'react';
import PropTypes from 'prop-types';


export default class Field extends React.Component {
    static propTypes = {
        label: PropTypes.string,
        subLabel: PropTypes.string,
    };

    static defaultProps = {
        label: null,
        subLabel: null
    };

    render() {
        return (
            <div className="field">
                {this.props.label &&
                    <div className="label">
                        {this.props.label}
                        {this.props.subLabel &&
                            <span className="has-text-grey"> {this.props.subLabel}</span>
                        }
                    </div>
                }

                <div className="control">
                    {this.props.children}
                </div>
            </div>
        );
    }
}
