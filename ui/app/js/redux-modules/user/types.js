export const USERS_REQUEST = 'users/REQUEST';
export const USERS_RECEIVE = 'users/RECEIVE';
export const USERS_ERROR = 'users/ERROR';
