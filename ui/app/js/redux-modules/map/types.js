export const MAP_SET_CENTER = 'map/SET_CENTER';
export const MAP_SET_ZOOM = 'map/SET_ZOOM';
export const MAP_SET_BOUNDS = 'map/SET_BOUNDS';
export const MAP_SET_CURRENT_BOUNDS = 'map/SET_CURRENT_BOUNDS';
export const MAP_SET_TYPE = 'map/TOGGLE_TYPE';
