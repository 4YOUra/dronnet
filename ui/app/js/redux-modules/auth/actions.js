import * as types from './types';
import * as authService from '../../services/auth';
import {errorDispatchAndReject} from '../../utils/utils';
import * as userService from '../../services/user';


export function logInRequest() {
    return {
        type: types.AUTH_LOGIN_REQUEST
    }
}

export function logInError(message) {
    return {
        type: types.AUTH_LOGIN_ERROR,
        message
    }
}

export function logInAsync({username, password}) {
    return (dispatch) => {
        dispatch(logInRequest());

        return authService.logInAsync(username, password).then(
            (response) => {
                localStorage.setItem('token', response.data.token);
                return response;
            },
            errorDispatchAndReject(dispatch, logInError, 'Invalid username or password')
        );
    };
}

export function userReceive(user) {
    return {
        type: types.AUTH_USER_RECEIVE,
        user
    }
}

export function userError() {
    return {
        type: types.AUTH_USER_ERROR
    }
}

export function fetchUserAsync() {
    return (dispatch) => {
        return userService.fetchUserAsync().then(
            (response) => {
                dispatch(userReceive(response.data));
                return response;
            },
            errorDispatchAndReject(dispatch, userError)
        );
    };
}
