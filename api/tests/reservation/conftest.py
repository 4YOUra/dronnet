from copy import deepcopy
from datetime import timedelta

import pytest
from django.utils import timezone


DEFAULT_CIRCLE_RESERVATION_DATA = {
    "coordinates": [[53.933913, 27.597814]],
    "radius": 100,
}

DEFAULT_POLYGON_RESERVATION_DATA = {
    "coordinates": [
        [53.933913, 27.597814],
        [53.934669, 27.598424],
        [53.934563, 27.598981],
        [53.933800, 27.598352]
    ],
}


def extend_reservation_data(data, drone):
    data['startDate'] = (timezone.now() + timedelta(days=1)).isoformat()
    data['endDate'] = (timezone.now() + timedelta(days=1, hours=3)).isoformat()
    data['droneIds'] = [drone.id]
    return data


@pytest.fixture
def valid_circle_res_data(db_user_drone):
    data = deepcopy(DEFAULT_CIRCLE_RESERVATION_DATA)
    extend_reservation_data(data, db_user_drone)
    return data


@pytest.fixture
def valid_polygon_res_data(db_user_drone):
    data = deepcopy(DEFAULT_POLYGON_RESERVATION_DATA)
    extend_reservation_data(data, db_user_drone)
    return data


@pytest.fixture
def invalid_res_data_without_radius(valid_circle_res_data):
    data = deepcopy(valid_circle_res_data)
    data.pop('radius')
    return data


@pytest.fixture
def invalid_res_data_too_many_coords(valid_circle_res_data):
    data = deepcopy(valid_circle_res_data)
    data['coordinates'] += data['coordinates']
    return data


@pytest.fixture
def invalid_res_data_coords_format(valid_polygon_res_data):
    data = deepcopy(valid_polygon_res_data)
    data['coordinates'][0].append(5)
    return data


@pytest.fixture
def invalid_res_data_coords_value(valid_polygon_res_data):
    data = deepcopy(valid_polygon_res_data)
    data['coordinates'][0] = [53.933913, -227.597814]
    return data


@pytest.fixture
def invalid_res_data_no_coords(valid_polygon_res_data):
    data = deepcopy(valid_polygon_res_data)
    data.pop('coordinates')
    return data


@pytest.fixture
def invalid_res_data_too_little_coords(valid_polygon_res_data):
    data = deepcopy(valid_polygon_res_data)
    data['coordinates'] = data['coordinates'][:2]
    return data


@pytest.fixture
def invalid_res_data_in_the_past(valid_polygon_res_data):
    data = deepcopy(valid_polygon_res_data)
    data['startDate'] = (timezone.now() - timedelta(days=1, hours=2)).isoformat()
    data['endDate'] = (timezone.now() - timedelta(days=1, hours=1)).isoformat()
    return data


@pytest.fixture
def invalid_res_data_too_long(valid_polygon_res_data):
    data = deepcopy(valid_polygon_res_data)
    data['startDate'] = (timezone.now() + timedelta(days=1)).isoformat()
    data['endDate'] = (timezone.now() + timedelta(days=3)).isoformat()
    return data


@pytest.fixture
def invalid_res_data_end_prior_to_start(valid_polygon_res_data):
    data = deepcopy(valid_polygon_res_data)
    data['startDate'], data['endDate'] = data['endDate'], data['startDate']
    return data


@pytest.fixture
def invalid_res_data_too_large(valid_polygon_res_data):
    data = deepcopy(valid_polygon_res_data)
    data['coordinates'] = [
        [-35.0, 115.0],
        [-15.0, 115.0],
        [-15.0, 155.0],
        [-35.0, 155.0]
    ]
    return data


@pytest.fixture
def invalid_res_data_no_drones(valid_polygon_res_data):
    data = deepcopy(valid_polygon_res_data)
    data['droneIds'] = []
    return data


@pytest.fixture
def invalid_res_data_wrong_drone(valid_polygon_res_data):
    data = deepcopy(valid_polygon_res_data)
    data['droneIds'] = [8848]
    return data


@pytest.fixture
def reservation_query_params_dates(valid_circle_res_data):
    start = valid_circle_res_data['startDate'].replace('+00:00', 'Z')
    end = valid_circle_res_data['endDate'].replace('+00:00', 'Z')
    return 'startDate={}&endDate{}'.format(start, end)


@pytest.fixture
def reservation_query_params_full_map(reservation_query_params_dates):
    return '?west=-180&north=90&east=180&south=-90&{}'.format(reservation_query_params_dates)


@pytest.fixture
def reservation_query_params_on_edge(reservation_query_params_dates):
    return '?west=40&north=51&east=-110&south=-73&{}'.format(reservation_query_params_dates)
