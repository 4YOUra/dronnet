import pytest

from rest_framework import status


@pytest.mark.django_db
def test_successful_login(anon_client, db_user, valid_credentials):
    response = anon_client.post('/api/login/', valid_credentials)

    assert response.status_code == status.HTTP_200_OK
    assert 'token' in response.json()


@pytest.mark.django_db
def test_unsuccessful_login(anon_client, invalid_credentials):
    response = anon_client.post('/api/login/', invalid_credentials)
    response_data = response.json()

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert 'non_field_errors' in response_data
    assert response_data['non_field_errors'] == ['Unable to log in with provided credentials.']


@pytest.mark.django_db
@pytest.mark.parametrize(
    'field', ['username', 'password']
)
def test_required_field(anon_client, field, required_field_data):
    response = anon_client.post('/api/login/', required_field_data)
    response_data = response.json()

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert field in response_data
    assert response_data[field] == ['This field is required.']
