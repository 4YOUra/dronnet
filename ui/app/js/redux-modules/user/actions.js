import * as types from './types';
import {errorDispatchAndReject} from '../../utils/utils';
import * as userService from '../../services/user';


export function usersRequest() {
    return {
        type: types.USERS_REQUEST
    }
}

export function usersReceive(items) {
    return {
        type: types.USERS_RECEIVE,
        items
    }
}

export function usersError(message) {
    return {
        type: types.USERS_ERROR,
        message
    }
}

export function fetchUsersAsync() {
    return (dispatch) => {
        dispatch(usersRequest());

        return userService.fetchUsersAsync().then(
            (response) => {
                dispatch(usersReceive(response.data));
                return response;
            },
            errorDispatchAndReject(dispatch, usersError, 'Unable to load drone models')
        );
    }
}
