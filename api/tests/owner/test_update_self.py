import pytest

from rest_framework import status


@pytest.mark.django_db
def test_update_phone(user_client, db_user, db_owner):
    new_phone = '+3755555555'
    response = user_client.patch('/api/owners/', {'phone': new_phone})
    response_data = response.json()

    assert response.status_code == status.HTTP_200_OK
    assert response_data['phone'] == new_phone


@pytest.mark.django_db
def test_update_first_name(user_client):
    new_first_name = 'Stanislau'
    response = user_client.patch('/api/owners/', {'firstName': new_first_name})
    response_data = response.json()

    assert response.status_code == status.HTTP_200_OK
    assert response_data['firstName'] == new_first_name


@pytest.mark.django_db
def test_update_username(user_client):
    new_username = 'adminadmin'
    response = user_client.patch('/api/owners/', {'username': new_username})
    response_data = response.json()

    assert response.status_code == status.HTTP_200_OK
    assert response_data['username'] != new_username
