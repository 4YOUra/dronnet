import moment from 'moment-timezone';
import {store} from '../App';

const regexIso8601 = /^(\d{4}|\+\d{6})-(\d{2})-(\d{2})(?:T(\d{2}):(\d{2}):(\d{2})(\.(\d{1,}))?(Z|([\-+])(\d{2}):(\d{2}))?)?$/;

// CONVERT TO MOMENT

export function convertToMoment(data) {
    return convertObjectToMoment({data: data}).data;
}

function convertArrayToMoment(arr) {
    for (const index of arr.keys()) {
        let child = arr[index];

        if (isDateFormat(child)) {
            arr[index] = convertStringToMoment(child);
        } else {
            convertNonDateToMoment(child);
        }
    }

    return arr;
}

function convertObjectToMoment(obj) {
    for (const key in obj) {
        if (!obj.hasOwnProperty(key)) {
            continue;
        }

        let child = obj[key];

        if (isDateFormat(child)) {
            obj[key] = convertStringToMoment(child);
        } else {
            convertNonDateToMoment(child);
        }
    }

    return obj;
}

function convertNonDateToMoment(obj) {
    if (Array.isArray(obj)) {
        convertArrayToMoment(obj);
    } else if (typeof obj === 'object') {
        convertObjectToMoment(obj);
    }
}

function convertStringToMoment(s) {
    return moment.tz(s, store.getState().auth.user.timezone);
}

function isDateFormat(obj) {
    return typeof obj === 'string' && obj.match(regexIso8601);
}


// CONVERT FROM MOMENT

export function convertFromMoment(data) {
    return convertObjectFromMoment({data: data}).data;
}

function convertArrayFromMoment(arr) {
    for (const index of arr.keys()) {
        let child = arr[index];

        if (isMomentObj(child)) {
            arr[index] = convertMomentToString(child);
        } else {
            convertNonDateFromMoment(child);
        }
    }

    return arr;
}

function convertObjectFromMoment(obj) {
    for (const key in obj) {
        if (!obj.hasOwnProperty(key)) {
            continue;
        }

        let child = obj[key];

        if (isMomentObj(child)) {
            obj[key] = convertMomentToString(child);
        } else {
            convertNonDateFromMoment(child);
        }
    }

    return obj;
}

function convertNonDateFromMoment(obj) {
    if (Array.isArray(obj)) {
        convertArrayFromMoment(obj);
    } else if (typeof obj === 'object') {
        convertObjectFromMoment(obj);
    }
}

function convertMomentToString(obj) {
    return obj.toISOString();
}

function isMomentObj(obj) {
    return !!obj && !!obj.toISOString;
}
