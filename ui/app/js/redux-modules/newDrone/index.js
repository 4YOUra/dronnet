import * as types from "./types";


const initialState = {
    errorMessage: '',
    isFetching: false,
    isFetchingDetails: false,
    isFetchingDelete: false,
    model: 0,
    name: '',
};


export default function newDrone(state = initialState, action) {
    switch (action.type) {

        case types.NEW_DRONE_REQUEST:
            return {
                ...state,
                isFetching: true
            };

        case types.NEW_DRONE_RECEIVE:
            return {
                ...state,
                isFetching: false,
                errorMessage: ''
            };

        case types.NEW_DRONE_ERROR:
            return {
                ...state,
                isFetching: false,
                errorMessage: action.message
            };

        case types.NEW_DRONE_REQUEST_DETAILS:
            return {
                ...state,
                isFetchingDetails: true
            };

        case types.NEW_DRONE_RECEIVE_DETAILS:
            return {
                ...state,
                isFetchingDetails: false,
                errorMessage: ''
            };

        case types.NEW_DRONE_ERROR_DETAILS:
            return {
                ...state,
                isFetchingDetails: false,
                errorMessage: action.message
            };

        case types.NEW_DRONE_REQUEST_DELETE:
            return {
                ...state,
                isFetchingDelete: true
            };

        case types.NEW_DRONE_RECEIVE_DELETE:
            return {
                ...state,
                isFetchingDelete: false,
                errorMessage: ''
            };

        case types.NEW_DRONE_ERROR_DELETE:
            return {
                ...state,
                isFetchingDelete: false,
                errorMessage: action.message
            };

        case types.NEW_DRONE_SET_MODEL:
            return {
                ...state,
                model: action.model
            };

        case types.NEW_DRONE_SET_NAME:
            return {
                ...state,
                name: action.name
            };

        case types.NEW_DRONE_RESET_FORM:
            return initialState;

        default:
            return state
    }
}
