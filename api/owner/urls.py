from django.conf.urls import url

from api.owner.models import DroneOwnerProfile
from api.owner.views import DroneOwnerProfileView, DroneOwnersView

urlpatterns = [
    url(r'^owners/$', DroneOwnerProfileView.as_view(), name='owners'),
    url(r'^owners/all/$', DroneOwnersView.as_view(), name='owners-all'),
    url(r'^owner-types/$', DroneOwnerProfile.Types.as_view(), name='owner-types'),
]
