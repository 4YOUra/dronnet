import React from 'react';
import {connect} from 'react-redux';
import {Redirect, Route} from 'react-router-dom';


@connect(
    (state) => ({
        user: state.auth.user
    })
)
export default class RouteAuthenticated extends React.Component {
    render() {
        return this.props.user.anon ? <Redirect to="/login"/> : <Route {...this.props}/>
    }
}
