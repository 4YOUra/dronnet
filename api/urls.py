from os import listdir
from os.path import join, isdir

from django.conf.urls import include, url

from api.apps import ApiConfig
from dronnet.settings import BASE_DIR

API_DIR = join(BASE_DIR, ApiConfig.name)

# Find all urls.py in api modules.
url_modules = ['{}.{}.urls'.format(ApiConfig.name, path)
               for path in listdir(API_DIR)
               if isdir(join(API_DIR, path)) and 'urls.py' in listdir(join(API_DIR, path))]

app_name = 'api'
urlpatterns = [url(r'^', include(module)) for module in url_modules]
