import React from 'react';
import PropTypes from 'prop-types';
import s from './style.scss';
import {getFormData} from '../../../utils/utils';
import cn from 'classnames';
import SubmitButton from '../../utils/SubmitButton/SubmitButton';
import HelpMessage from '../../utils/HelpMessage/HelpMessage';
import Field from "../../utils/Field/Field";
import Input from "../../utils/Input/Input";


export default class LogIn extends React.Component {
    static propTypes = {
        errorMessage: PropTypes.string,
        isFetching: PropTypes.bool,
        isSignedUp: PropTypes.bool,
        onFormSubmit: PropTypes.func.isRequired
    };

    static defaultProps = {
        errorMessage: '',
        isFetching: false,
        isSignedUp: false
    };

    handleFormSubmit = (event) => {
        event.preventDefault();
        const formData = getFormData(event.target);
        this.props.onFormSubmit(formData);
    };

    render() {
        return (
            <form className="form is-dark" onSubmit={this.handleFormSubmit}>
                <Field label="Username">
                    <Input placeholder="JohnDoe" name="username"/>
                </Field>

                <Field label="Password">
                    <Input type="password" placeholder="••••••••••••" name="password"/>
                </Field>

                <SubmitButton isFetching={this.props.isFetching}>Log In</SubmitButton>

                <HelpMessage error>
                    {this.props.errorMessage}
                </HelpMessage>

                {this.props.isSignedUp &&
                    <HelpMessage color="grey" size="5">
                        You have successfully signed up! You can now log into your new account.
                    </HelpMessage>
                }
            </form>
        );
    }
}
