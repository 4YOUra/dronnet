import {connect} from 'react-redux';
import React from 'react';
import DroneModelForm from "./DroneModelForm/DroneModelForm";
import {push} from 'react-router-redux';
import {
    createDroneModelAsync, deleteDroneModelAsync, newDroneModelResetForm, newDroneModelSetManufacturer,
    newDroneModelSetModelName,
    setDroneModelForEditAsync, updateDroneModelAsync
} from "../../redux-modules/newDroneModel/actions";
import {CREATE_MODE, UPDATE_MODE} from "../../utils/constants";


@connect(
    (state) => ({
        errorMessage: state.newDroneModel.errorMessage,
        isFetching: state.newDroneModel.isFetching,
        isFetchingDetails: state.newDroneModel.isFetchingDetails,
        isFetchingDelete: state.newDroneModel.isFetchingDelete,
        manufacturer: state.newDroneModel.manufacturer,
        modelName: state.newDroneModel.modelName,
    }),
    {
        createDroneModelAsync,
        deleteDroneModelAsync,
        newDroneModelResetForm,
        setDroneModelForEditAsync,
        updateDroneModelAsync,
        changeRoute: push,
        onManufacturerChange: newDroneModelSetManufacturer,
        onModelNameChange: newDroneModelSetModelName,
    }
)
export default class DroneModelFormContainer extends React.Component {
    constructor(props) {
        super(props);
        this.editMode = this.props.match.params.droneModelId ? UPDATE_MODE : CREATE_MODE;
    }

    componentDidMount() {
        if (this.editMode === UPDATE_MODE) {
            this.droneModelId = parseInt(this.props.match.params.droneModelId);
            this.props.setDroneModelForEditAsync(this.droneModelId);
        }
    }

    componentWillUnmount() {
        this.props.newDroneModelResetForm();
    }

    handleDeleteButtonClick = () => {
        this.props.deleteDroneModelAsync(this.droneModelId).then(
            () => this.props.changeRoute('/admin/drone-models')
        );
    };

    handleFormSubmit = () => {
        if (this.editMode === CREATE_MODE) {
            this.props.createDroneModelAsync().then(
                () => this.props.changeRoute('/admin/drone-models')
            );
        }

        if (this.editMode === UPDATE_MODE) {
            this.props.updateDroneModelAsync(this.droneModelId).then(
                () => this.props.changeRoute('/admin/drone-models')
            );
        }
    };

    render() {
        return (
            <DroneModelForm
                {...this.props}
                editMode={this.editMode}
                onDeleteButtonClick={this.handleDeleteButtonClick}
                onFormSubmit={this.handleFormSubmit}
            />
        )
    }
}
