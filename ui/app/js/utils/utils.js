import {HOVER_FILL_COLOR, HOVER_STROKE_COLOR} from "../redux-modules/reservation/constants";

String.prototype.trunc = String.prototype.trunc ||
    function (n) {
        return (this.length > n) ? this.substr(0, n - 1) + '…' : this;
    };

export function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

export function getErrorMessage(response, defaultMessage = 'Error') {
    if (!response) {
        return 'Connection error. Try again later.';
    }

    const data = response.data;

    if (response.status === 401) {
        return 'You have to log in.'
    }

    if (data.non_field_errors || data.nonFieldErrors) {
        return data.nonFieldErrors ? data.nonFieldErrors[0] : data.non_field_errors[0];
    }

    const errorKey = Object.keys(data)[0];

    if (errorKey) {
        if (Array.isArray(data[errorKey])) {
            return `${capitalizeFirstLetter(errorKey)}: ${data[errorKey][0]}`;
        } else {
            return data[errorKey];
        }
    }

    return defaultMessage;
}

export function getFormData(form) {
    const formData = new FormData(form);

    let data = {};
    formData.forEach(function (value, key) {
        data[key] = value;
    });

    return data;
}

export function getBoundsForCoordinates(coordinates) {
    let bounds = new google.maps.LatLngBounds();

    for (let coordinate of coordinates) {
        bounds.extend(coordinate);
    }

    return bounds.toJSON();
}

export function getCoordinatesFromLatLngArray(latLngArray) {
    let coordinates = [];

    for (let latLng of latLngArray) {
        coordinates.push({lat: latLng.lat(), lng: latLng.lng()});
    }

    return coordinates;
}

export function getCoordinatesFromPolygon(polygon) {
    return getCoordinatesFromLatLngArray(polygon.getPath().getArray())
}

export function coordinatesToTuples(coordinates) {
    let tuples = [];

    for (let c of coordinates) {
        tuples.push([c.lat, c.lng]);
    }

    return tuples;
}

export function getBoundsParamsFromState(state) {
    let bounds = state.map.currentBounds;

    // If bounds are not set, use full map.
    return bounds || {east: 180, north: 90, south: -90, west: -180};
}

export function errorDispatchAndReject(dispatch, action, defaultMessage) {
    return (error) => {
        const message = getErrorMessage(error.response, defaultMessage);
        dispatch(action(message));
        return Promise.reject(error);
    };
}

export function reservationWithColor(reservation, isHovered) {
    return {
        ...reservation,
        fillColor: isHovered ? HOVER_FILL_COLOR : enums.reservationHeights.fillColors[reservation.height],
        strokeColor: isHovered? HOVER_STROKE_COLOR : enums.reservationHeights.strokeColors[reservation.height]
    }
}
