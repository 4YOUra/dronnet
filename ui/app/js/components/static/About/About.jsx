import React from 'react';
import SectionTitle from "../../utils/SectionTitle/SectionTitle";
import {Link} from "react-router-dom";


export default class About extends React.Component {
    render() {
        return (
            <div className="content">
                <SectionTitle text="About"/>

                <p>
                    DRONNET is a platform for creating reservations of air space for driving drones.
                    All you need to do is
                    <Link to="/signup"> sign up</Link> and <Link to="/create-reservation">create new reservation</Link>.
                </p>

                <SectionTitle text="History"/>

                <p>
                    This project was originally started
                    within <span className="has-text-weight-bold">NASA Space Apps Challenge</span> in Minsk.
                </p>
                <p>
                    Initial stack:
                    <ul>
                        <li>Django (+ REST)</li>
                        <li>PostgreSQL</li>
                        <li>Angular</li>
                    </ul>
                </p>
                <p>
                    Current stack:
                    <ul>
                        <li>Django (+ REST)</li>
                        <li>PostgreSQL (+ PostGIS)</li>
                        <li>React</li>
                    </ul>
                </p>
            </div>
        );
    }
}
