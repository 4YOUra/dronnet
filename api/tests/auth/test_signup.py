import pytest
from rest_framework import status


@pytest.mark.django_db
def test_successful_signup(anon_client, valid_signup_data):
    response = anon_client.post('/api/signup/', valid_signup_data)

    assert response.status_code == status.HTTP_200_OK
    assert 'token' in response.json()


@pytest.mark.django_db
def test_invalid_email(anon_client, invalid_email_data):
    response = anon_client.post('/api/signup/', invalid_email_data)
    response_data = response.json()

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert 'email' in response_data
    assert response_data['email'] == ['Enter a valid email address.']


@pytest.mark.django_db
@pytest.mark.parametrize(
    'field', ['username', 'password', 'email']
)
def test_required_field(anon_client, field, required_field_data):
    response = anon_client.post('/api/signup/', required_field_data)
    response_data = response.json()

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert field in response_data
    assert response_data[field] == ['This field is required.']
