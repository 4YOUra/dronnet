import React from 'react';
import DroneModelsList from './DroneModelsList/DroneModelsList';
import {connect} from 'react-redux';
import {fetchDroneModelsAsync} from '../../redux-modules/droneModel/actions';
import {push} from "react-router-redux";


@connect(
    (state) => ({
        droneModels: state.droneModel.items,
        errorMessage: state.droneModel.errorMessage,
        isFetching: state.droneModel.isFetching
    }),
    {
        fetchDroneModelsAsync,
        changeRoute: push,
    }
)
export default class DroneModelsListContainer extends React.Component {
    componentDidMount() {
        this.props.fetchDroneModelsAsync();
    }

    handleBoxClick = (droneModel) => {
        this.props.changeRoute(`/admin/update-drone-model/${droneModel.id}`);
    };

    render() {
        return (
            <DroneModelsList
                {...this.props}
                onBoxClick={this.handleBoxClick}
            />
        );
    }
}
