import * as types from './types';
import moment from 'moment-timezone';
import {FILTER_ALL} from './constants';
import {reservationWithColor} from "../../utils/utils";


const initialState = {
    errorMessage: '',
    isFetching: false,
    items: [],
    dateFilter: moment(),
    filterMode: FILTER_ALL,
    hoveredItemId: null,
};


export default function reservation(state = initialState, action) {
    switch (action.type) {

        case types.RESERVATIONS_REQUEST:
            return {
                ...state,
                isFetching: true
            };

        case types.RESERVATIONS_RECEIVE:
            return {
                ...state,
                isFetching: false,
                errorMessage: '',
                items: action.items.map((item) => reservationWithColor(item, item.id === state.hoveredItemId))
            };

        case types.RESERVATIONS_ERROR:
            return {
                ...state,
                isFetching: false,
                errorMessage: action.message
            };

        case types.RESERVATIONS_SET_DATE_FILTER:
            return {
                ...state,
                dateFilter: action.dateFilter
            };

        case types.RESERVATIONS_SET_FILTER_MODE:
            return {
                ...state,
                filterMode: action.filterMode
            };

        case types.RESERVATIONS_HOVER:
            return {
                ...state,
                items: state.items.map((item) => reservationWithColor(item, item.id === action.itemId)),
                hoveredItemId: action.itemId,
            };

        default:
            return state
    }
}
