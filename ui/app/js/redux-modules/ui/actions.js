import * as types from './types';

export function uiOpenSection() {
    return {
        type: types.UI_OPEN_SECTION
    };
}

export function uiCloseSection() {
    return {
        type: types.UI_CLOSE_SECTION
    };
}

export function uiToggleSection() {
    return (dispatch, getState) => {
        if (getState().ui.isSectionOpen) {
            dispatch(uiCloseSection());
        } else {
            dispatch(uiOpenSection());
        }
    }
}

export function uiOpenLegend() {
    return {
        type: types.UI_OPEN_LEGEND
    };
}

export function uiCloseLegend() {
    return {
        type: types.UI_CLOSE_LEGEND
    };
}

export function uiToggleLegend() {
    return (dispatch, getState) => {
        if (getState().ui.isLegendOpen) {
            dispatch(uiCloseLegend());
            localStorage.removeItem('isLegendOpen');
        } else {
            dispatch(uiOpenLegend());
            localStorage.setItem('isLegendOpen', true);
        }
    }
}
