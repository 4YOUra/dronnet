import {connect} from 'react-redux';
import React from "react";
import Header from './Header/Header';
import {uiToggleSection} from '../../redux-modules/ui/actions';
import * as authService from '../../services/auth';
import {mapSetType} from '../../redux-modules/map/actions';


@connect(
    (state) => ({
        isBurgerActive: state.ui.isSectionOpen,
        mapType: state.map.mapType,
        path: state.router.location.pathname,
        user: state.auth.user
    }),
    {
        mapSetType,
        onBurgerClick: uiToggleSection
    }
)
export default class HeaderContainer extends React.Component {
    handleLogOutClick = () => {
        authService.logOut();
        window.location.href = '/';
    };

    handleMapTypeClick = () => {
        const newMapType = this.props.mapType === 'roadmap' ? 'satellite' : 'roadmap';
        localStorage.setItem('mapType', newMapType);
        this.props.mapSetType(newMapType);
    };

    render() {
        return (
            <Header
                {...this.props}
                onLogOutClick={this.handleLogOutClick}
                onMapTypeClick={this.handleMapTypeClick}
            />
        );
    }
}
