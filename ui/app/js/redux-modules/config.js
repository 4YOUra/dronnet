import axios from 'axios';
import {convertFromMoment, convertToMoment} from '../utils/dateConverter';

axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';

axios.interceptors.request.use((config) => {
    const token = localStorage.getItem('token');
    if (token) { config.headers.Authorization = `Token ${token}`; }
    config.data = convertFromMoment(config.data);
    config.params = convertFromMoment(config.params);
    return config;
});

axios.interceptors.response.use((response) => {
    response.data = convertToMoment(response.data);
    return response;
});

export const apiUrl = process.env.API_URL;
