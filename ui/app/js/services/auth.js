import axios from 'axios';
import {apiUrl} from '../redux-modules/config';

export function logInAsync(username, password) {
    return axios.post(`${apiUrl}/login/`, {username, password});
}

export function logOut() {
    localStorage.removeItem('token');
}

export function signUpAsync(email, username, password) {
    return axios.post(`${apiUrl}/signup/`, {email, username, password});
}
