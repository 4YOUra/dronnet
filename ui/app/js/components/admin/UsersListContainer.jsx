import React from 'react';
import {connect} from 'react-redux';
import {fetchUsersAsync} from "../../redux-modules/user/actions";
import UsersList from "./UsersList/UsersList";


@connect(
    (state) => ({
        users: state.user.items,
        errorMessage: state.user.errorMessage,
        isFetching: state.user.isFetching
    }),
    {
        fetchUsersAsync,
    }
)
export default class UsersListContainer extends React.Component {
    componentDidMount() {
        this.props.fetchUsersAsync();
    }

    render() {
        return (
            <UsersList
                {...this.props}
            />
        );
    }
}
