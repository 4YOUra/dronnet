from django.conf.urls import url

from api.reservation.models import Reservation
from api.reservation.views import ReservationView, ReservationDetailsView

urlpatterns = [
    url(r'^reservations/$', ReservationView.as_view(), name='reservations'),
    url(r'^reservations/own/$', ReservationView.as_view(only_own=True), name='reservations-own'),
    url(r'^reservations/(?P<pk>\d+)/$', ReservationDetailsView.as_view(), name='reservation-details'),
    url(r'^reservation-heights/$', Reservation.Heights.as_view(), name='reservation-heights'),
]
