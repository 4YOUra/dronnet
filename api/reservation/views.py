from djangorestframework_camel_case.util import underscoreize
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated, IsAdminUser

from api.reservation.models import Reservation
from api.reservation.serializers import ReservationSerializer, TrafficRequestSerializer, ReservationDetailsSerializer


class ReservationView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer

    only_own = False

    def get_queryset(self):
        serializer = TrafficRequestSerializer(data=underscoreize(self.request.query_params))
        serializer.is_valid(raise_exception=True)

        reservations = Reservation.objects.get_traffic(**serializer.validated_data)
        if self.only_own:
            reservations = reservations.filter(owner__user=self.request.user)

        return reservations

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user.profile)


class ReservationDetailsView(generics.RetrieveUpdateAPIView):
    permission_classes = (IsAdminUser,)
    serializer_class = ReservationDetailsSerializer
    queryset = Reservation.objects.all()
