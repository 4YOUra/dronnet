import {connect} from 'react-redux';
import React from "react";
import LogIn from "./LogIn/LogIn";
import {logInAsync} from '../../redux-modules/auth/actions';
import {push} from 'react-router-redux';


@connect(
    (state) => ({
        errorMessage: state.auth.errorMessage,
        isFetching: state.auth.isFetching,
        isSignedUp: state.signUp.isSignedUp
    }),
    { logInAsync, changeRoute: push }
)
export default class LogInContainer extends React.Component {
    handleFormSubmit = (data) => {
        this.props.logInAsync(data).then(
            () => window.location.href = '/'
        );
    };

    render() {
        return (
            <LogIn
                {...this.props}
                onFormSubmit={this.handleFormSubmit}
            />
        );
    }
}
