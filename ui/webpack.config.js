const path = require('path');
const webpack = require('webpack');

const HtmlWebPackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');


const paths = {
    app: path.resolve(__dirname, 'app'),
    build: path.resolve(__dirname, 'build')
};


module.exports = {
    entry: path.resolve(paths.app, 'js/App.jsx'),

    output: {
        path: paths.build,
        publicPath: '/static/',
        filename: 'js/[name].js'
    },

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.html$/,
                use: 'html-loader'
            },
            {
                test: /\.(sass|scss)$/,
                exclude: [/\.global/],
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true,
                                localIdentName: '[name]__[local]___[hash:base64:5]'
                            }
                        },
                        {
                            loader: 'sass-loader'
                        }
                    ]
                })
            },
            {
                test: /\.(sass|scss)$/,
                include: [/\.global/],
                use: ExtractTextPlugin.extract({
                    use: ['css-loader', 'sass-loader']
                })
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: 'file-loader'
            }
        ]
    },

    plugins: [
        new HtmlWebPackPlugin({
            template: path.resolve(paths.app, 'index.html'),
            filename: 'index.html'
        }),
        new CopyWebpackPlugin([{
            from: path.resolve(paths.app, 'images/dronnetfavicon.png'),
            to: path.resolve(paths.build, 'favicon.png')
        }]),
        new ExtractTextPlugin('styles/main.css'),
        new webpack.EnvironmentPlugin({
            API_URL: 'http://localhost:8888/api'
        })
    ],

    resolve: {
        extensions: ['.js', '.jsx'],
        alias: {
            Images: path.resolve(paths.app, 'images')
        }
    }
};
