from copy import deepcopy

import pytest

DEFAULT_DRONE_MODEL_DATA = {
    'manufacturer': 'DJI',
    'modelName': 'Phantom 4'
}

DEFAULT_DRONE_DATA = {
    'name': 'My DJI'
}


@pytest.fixture
def valid_drone_model_data():
    return deepcopy(DEFAULT_DRONE_MODEL_DATA)


@pytest.fixture
def valid_drone_data(db_drone_model):
    data = deepcopy(DEFAULT_DRONE_DATA)
    data['model'] = db_drone_model.id
    return data
