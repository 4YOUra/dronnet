import React from 'react';
import {GoogleMap, withGoogleMap} from 'react-google-maps';
import {compose, withProps} from "recompose";
import s from './style.scss';


const GoogleMaps = compose(
    withProps({
        loadingElement: <div className={s.mapContainer}/>,
        containerElement: <div/>,
        mapElement: <div className={s.mapContainer}/>,
    }),
    withGoogleMap
)(props => {
    const {onMapMounted, ...otherProps} = props;
    return (
        <GoogleMap {...otherProps} ref={c => {
            onMapMounted && onMapMounted(c)
        }}>
            {props.children}
        </GoogleMap>
    );
});

export default GoogleMaps;
