import * as types from "./types";


const initialState = {
    errorMessage: '',
    successMessage: '',
    isFetching: false,
    firstName: '',
    lastName: '',
    address: '',
    phone: '',
    timezone: '',
};


export default function account(state = initialState, action) {
    switch (action.type) {

        case types.ACCOUNT_REQUEST:
            return {
                ...state,
                isFetching: true,
                successMessage: '',
            };

        case types.ACCOUNT_RECEIVE:
            return {
                ...state,
                isFetching: false,
                errorMessage: '',
                successMessage: 'Successfully updated',
            };

        case types.ACCOUNT_ERROR:
            return {
                ...state,
                isFetching: false,
                errorMessage: action.message
            };

        case types.ACCOUNT_SET_FIRST_NAME:
            return {
                ...state,
                firstName: action.firstName,
            };

        case types.ACCOUNT_SET_LAST_NAME:
            return {
                ...state,
                lastName: action.lastName,
            };

        case types.ACCOUNT_SET_ADDRESS:
            return {
                ...state,
                address: action.address,
            };

        case types.ACCOUNT_SET_PHONE:
            return {
                ...state,
                phone: action.phone,
            };

        case types.ACCOUNT_SET_TIMEZONE:
            return {
                ...state,
                timezone: action.timezone,
            };

        case types.ACCOUNT_RESET_FORM:
            return initialState;

        default:
            return state
    }
}
