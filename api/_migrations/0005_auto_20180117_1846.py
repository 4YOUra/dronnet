# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2018-01-17 18:46
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_auto_20160927_0024'),
    ]

    operations = [
        migrations.CreateModel(
            name='DroneModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('manufacturer', models.CharField(max_length=50)),
                ('model_name', models.CharField(blank=True, max_length=127)),
            ],
        ),
        migrations.AlterField(
            model_name='drone',
            name='model',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='api.DroneModel'),
            preserve_default=False,
        ),
    ]
