import React from 'react';
import s from './style.scss';
import cn from 'classnames';
import PropTypes from 'prop-types';
import Conditional from "../../utils/Conditional/Conditional";


export default class ReservationBox extends React.Component {
    static propTypes = {
        center: PropTypes.object.isRequired,
        coordinates: PropTypes.array.isRequired,
        drones: PropTypes.array.isRequired,
        endDate: PropTypes.object.isRequired,
        height: PropTypes.number.isRequired,
        id: PropTypes.number.isRequired,
        onEditButtonClick: PropTypes.func.isRequired,
        onReservationClick: PropTypes.func.isRequired,
        onReservationHover: PropTypes.func.isRequired,
        ownerUsername: PropTypes.string.isRequired,
        startDate: PropTypes.object.isRequired,
        userType: PropTypes.string.isRequired,
    };

    getDate() {
        return this.props.startDate.format('MMM Do YYYY');
    }

    getTimeInterval() {
        return this.props.startDate.format('HH:mm') + ' - ' + this.props.endDate.format('HH:mm');
    }

    handleClick = () => {
        this.props.onReservationClick(this.props);
    };

    handleEditButtonClick = (e) => {
        e.stopPropagation();
        this.props.onEditButtonClick(this.props);
    };

    handleHover = () => {
        this.props.onReservationHover(this.props);
    };

    handleUnhover = () => {
        this.props.onReservationHover(null);
    };

    render() {
        return (
            <div className={cn("box", s.reservationBox)}
                 onClick={this.handleClick}
                 onMouseEnter={this.handleHover}
                 onMouseLeave={this.handleUnhover}
            >
                <div className="media">
                    <div className={cn("media-left has-text-centered", s.media70px)}>
                        <span className="is-size-4 has-text-weight-bold">#{this.props.id}</span>
                        <br/>
                        <small
                            className="tooltip is-tooltip-right"
                            data-tooltip={this.props.drones.join(', ') || 'No drones (deprecated)'}>
                            @<span className={s.drones}>{this.props.ownerUsername.trunc(8)}</span>
                        </small>
                    </div>

                    <div className="media-content">
                        <div className="content">
                            <small>{this.getDate()}</small>
                            <br/>
                            <small>{this.getTimeInterval()}</small>
                            <br/>
                            <small>{enums.reservationHeights.shortLabels[this.props.height]} height</small>
                        </div>
                    </div>

                    <Conditional if={this.props.userType === enums.ownerTypes.ADMIN}>
                        <button className={cn("button is-dark is-small", s.editButton)}
                                onClick={this.handleEditButtonClick}>
                            Edit
                        </button>
                    </Conditional>
                </div>
            </div>
        )
    }
}
