import pytest
from rest_framework import status


@pytest.mark.django_db
def test_retrieve_drone_models_by_regular_user(user_client):
    response = user_client.get('/api/drone-models/')
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_retrieve_drone_models_by_anon(anon_client):
    response = anon_client.get('/api/drone-models/')
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_retrieve_drone_model_by_id(user_client, db_drone_model):
    response = user_client.get('/api/drone-models/{}/'.format(db_drone_model.id))
    assert response.status_code == status.HTTP_200_OK
