import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import s from './style.scss';


export default class DroneModelBox extends React.Component {
    static propTypes = {
        id: PropTypes.number.isRequired,
        manufacturer: PropTypes.string.isRequired,
        modelName: PropTypes.string.isRequired,
        onBoxClick: PropTypes.func.isRequired,
    };

    render() {
        return (
            <div className={cn("box", s.box)} onClick={this.props.onBoxClick}>
                <p><span className="param-name">Manufacturer:</span> {this.props.manufacturer}</p>
                <p><span className="param-name">Model Name:</span> {this.props.modelName}</p>
            </div>
        );
    }
}
