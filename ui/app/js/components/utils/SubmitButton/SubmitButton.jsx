import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';


export default class SubmitButton extends React.Component {
    static propTypes = {
        isFetching: PropTypes.bool,
    };

    static defaultProps = {
        isFetching: false,
    };

    render() {
        return (
            <div className="field">
                <div className="control">
                    <button className={cn("button", {'is-loading': this.props.isFetching})}>
                        {this.props.children}
                    </button>
                </div>
            </div>
        );
    }
}
