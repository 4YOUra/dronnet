from datetime import timedelta

import pytest
from django.contrib.auth.models import User
from django.contrib.gis.geos import Polygon
from django.utils import timezone
from rest_framework.test import APIClient

from api.drone.models import DroneModel, Drone
from api.owner.models import DroneOwnerProfile
from api.reservation.models import Reservation
from api.utils import get_circle_coordinates

DEFAULT_USER_DATA = {
    'username': 'test_user',
    'password': 'test_password'
}

DEFAULT_OWNER_DATA = {
    'phone': 'test_phone',
    'address': 'test_address'
}

DEFAULT_ADMIN_DATA = {
    'username': 'test_admin',
    'email': 'admin@test.com',
    'password': 'test_admin_password'
}


@pytest.fixture(scope='session')
def anon_client():
    yield APIClient()


@pytest.fixture
def db_user():
    return User.objects.create_user(**DEFAULT_USER_DATA)


@pytest.fixture
def db_owner(db_user):
    return DroneOwnerProfile.objects.create(user=db_user)


@pytest.fixture
def db_admin():
    return User.objects.create_superuser(**DEFAULT_ADMIN_DATA)


@pytest.fixture
def db_admin_owner(db_admin):
    return DroneOwnerProfile.objects.create(user=db_admin, type=DroneOwnerProfile.Types.ADMIN)


@pytest.fixture
def user_client(anon_client, db_owner):
    anon_client.force_authenticate(user=db_owner.user)
    yield anon_client
    anon_client.force_authenticate(user=None)


@pytest.fixture
def admin_client(anon_client, db_admin_owner):
    anon_client.force_authenticate(user=db_admin_owner.user)
    yield anon_client
    anon_client.force_authenticate(user=None)


@pytest.fixture
def db_drone_model():
    return DroneModel.objects.create(manufacturer='DJI', model_name='Test')


@pytest.fixture
def db_user_drone(db_owner, db_drone_model):
    return Drone.objects.create(name='Dji', model=db_drone_model, owner=db_owner)


@pytest.fixture
def db_user_reservation(db_owner, db_user_drone):
    coordinates = get_circle_coordinates([45, 45], 50)
    coordinates += [coordinates[0]]
    reservation = Reservation.objects.create(
        zone=Polygon(coordinates),
        owner=db_owner,
        start_date=timezone.now() + timedelta(days=1),
        end_date=timezone.now() + timedelta(days=1, hours=3),
    )
    reservation.drones.add(db_user_drone)
    return reservation


@pytest.fixture
def db_user_reservation_australia(db_owner, db_user_drone):
    coordinates = get_circle_coordinates([151, -28], 50)
    coordinates += [coordinates[0]]
    reservation = Reservation.objects.create(
        zone=Polygon(coordinates),
        owner=db_owner,
        start_date=timezone.now() + timedelta(days=1),
        end_date=timezone.now() + timedelta(days=1, hours=3),
    )
    reservation.drones.add(db_user_drone)
    return reservation
