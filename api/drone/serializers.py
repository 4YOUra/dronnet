from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from api.drone.models import Drone, DroneModel


class DroneSerializer(serializers.ModelSerializer):

    class Meta:
        model = Drone
        exclude = ('owner',)
        read_only_fields = ('created_at',)


class DroneModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = DroneModel
        extra_kwargs = {
            'model_name': {
                'required': True,
                'allow_blank': False
            }
        }

    def get_from_data_or_instance(self, data, attr):
        return data[attr] if attr in data else getattr(self.instance, attr)

    def validate(self, data):
        model_name = self.get_from_data_or_instance(data, 'model_name')
        manufacturer = self.get_from_data_or_instance(data, 'manufacturer')

        if DroneModel.objects.filter(model_name=model_name, manufacturer=manufacturer).count():
            raise ValidationError('This model already exists')

        return data
