import axios from 'axios';
import {apiUrl} from '../redux-modules/config';

export function fetchDronesAsync() {
    return axios.get(`${apiUrl}/drones/`);
}

export function fetchDronesForUserAsync(userId) {
    return axios.get(`${apiUrl}/drones/`, {params: {userId}});
}

export function fetchDroneDetails(id) {
    return axios.get(`${apiUrl}/drones/${id}/`);
}

export function createDroneAsync({name, model}) {
    return axios.post(`${apiUrl}/drones/`, {name, model})
}

export function updateDroneAsync(id, {name, model}) {
    return axios.patch(`${apiUrl}/drones/${id}/`, {name, model})
}

export function deleteDroneAsync(id) {
    return axios.delete(`${apiUrl}/drones/${id}/`)
}
