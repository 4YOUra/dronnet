import React from 'react';
import PropTypes from 'prop-types';


export default class Conditional extends React.Component {
    static propTypes = {
        if: PropTypes.any,
    };

    render() {
        return this.props.if ? this.props.children : null;
    }
}
