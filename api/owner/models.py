import pytz
from django.contrib.auth.models import User
from django.contrib.gis.db import models

from api.utils import Enum


class DroneOwnerProfile(models.Model):

    class Types(Enum):
        COMMON = 'C'
        PREMIUM = 'P'
        ADMIN = 'A'

    Timezones = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    user = models.OneToOneField(User, related_name='profile')
    phone = models.CharField(max_length=15, blank=True)
    address = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    type = models.CharField(max_length=1, choices=Types.choices(), default=Types.COMMON)
    timezone = models.CharField(max_length=32, choices=Timezones, default='UTC')

    class Meta:
        ordering = ('id',)
