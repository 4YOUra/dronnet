import React from 'react';
import s from './style.scss';
import ReservationBox from '../ReservationBox/ReservationBox';
import PropTypes from 'prop-types';

export default class ReservationsList extends React.Component {
    static propTypes = {
        reservations: PropTypes.array.isRequired,
        onEditButtonClick: PropTypes.func.isRequired,
        onReservationClick: PropTypes.func.isRequired,
        onReservationHover: PropTypes.func.isRequired,
        userType: PropTypes.string.isRequired,
    };

    render() {
        const {reservations, ...otherProps} = this.props;
        return (
            <div>
                {reservations.map((reservation) =>
                    <ReservationBox
                        {...reservation}
                        {...otherProps}
                        key={reservation.id}
                    />
                )}
            </div>
        );
    }
}
